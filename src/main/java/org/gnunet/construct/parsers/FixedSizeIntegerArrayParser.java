/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;
import org.gnunet.construct.ProtocolViolationException;
import org.gnunet.construct.ReflectUtil;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.List;

public class FixedSizeIntegerArrayParser implements Parser {

    private final int byteSize;
    private final boolean signed;

    private final Field targetField;

    private final int elemNumber;

    public FixedSizeIntegerArrayParser(final int elemNumber, boolean signed, int byteSize, final Field f) {
        targetField = f;
        this.elemNumber = elemNumber;
        this.signed = signed;
        this.byteSize = byteSize;
    }

    @Override
    public int getSize(final Message srcObj) {
        return byteSize * elemNumber;
    }

    @Override
    public int parse(ByteBuffer srcBuf, int frameOffset,
                     Message frameObj, final Message dstObj, List<Field> frameSizePath) {
        int size = 0;

        @SuppressWarnings("unchecked")
        final Class<Message> arrayElementType = (Class<Message>) targetField.getType().getComponentType();

        if (!arrayElementType.isPrimitive()) {
            throw new AssertionError("IntegerFillParser is expected to be of primitive type, not " + arrayElementType);
        }

        final Object arr = Array.newInstance(targetField.getType().getComponentType(), elemNumber);
        ReflectUtil.justSet(dstObj, targetField, arr);

        for (int i = 0; i < elemNumber; ++i) {
            long v;
            try {
                v = IntegerUtil.readLong(srcBuf, signed, byteSize);
            } catch (BufferUnderflowException e) {
                throw new ProtocolViolationException("fixed size array underflow: " + targetField.toString());
            }
            ReflectUtil.justSetArray(arr, i, v);
        }

        return size;
    }

    @Override
    public int write(final ByteBuffer dstBuf,
                     final Message srcObj) {
        int size = 0;
        final Object arr = ReflectUtil.justGet(srcObj, targetField);
        if (Array.getLength(arr) != elemNumber) {
            throw new AssertionError("wrong number of elements");
        }
        for (int i = 0; i < Array.getLength(arr); ++i) {
            IntegerUtil.writeLong(Array.getLong(arr, i), dstBuf, signed, byteSize);
            size += byteSize;
        }
        return size;
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        // nothing to patch!
    }

    @Override
    public int getStaticSize() {
        return elemNumber * byteSize;
    }
}
