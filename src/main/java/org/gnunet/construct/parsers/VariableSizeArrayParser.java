/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;
import org.gnunet.construct.ReflectUtil;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.List;

public class VariableSizeArrayParser implements Parser {
    private final Field targetField;
    private final Parser elemParser;
    private ReflectUtil.NumField sizeField;


    public VariableSizeArrayParser(final Parser elemParser, Field sizeField, Field arrayField) {
        targetField = arrayField;
        this.elemParser = elemParser;
        this.sizeField = new ReflectUtil.NumField(sizeField);
    }

    @Override
    public int getSize(final Message src) {
        int size = 0;
        final Object arr = ReflectUtil.justGet(src, targetField);

        if (arr == null) {
            throw new RuntimeException("array not initialized");
        }

        for (int i = 0; i < Array.getLength(arr); ++i) {
            size += elemParser.getSize((Message) Array.get(arr, i));
        }
        return size;
    }

    @Override
    public int parse(final ByteBuffer srcBuf, int frameOffset, Message frameObj, final Message dstObj, List<Field>
            frameSizePath) {
        final int elemNumber = (int) sizeField.get(dstObj);

        @SuppressWarnings("unchecked")
        final Class<Message> arrayElementType = (Class<Message>) targetField.getType().getComponentType();

        int size = 0;

        final Object arr = Array.newInstance(arrayElementType, elemNumber);
        ReflectUtil.justSet(dstObj, targetField, arr);

        for (int i = 0; i < elemNumber; ++i) {
            Message elemObj;

            elemObj = ReflectUtil.justInstantiate(arrayElementType);

            Array.set(arr, i, elemObj);

            size += elemParser.parse(srcBuf, frameOffset - size, null, elemObj, null);
        }

        return size;
    }

    @Override
    public int write(final ByteBuffer dstBuf, final Message src) {
        int size = 0;
        final Object arr = ReflectUtil.justGet(src, targetField);
        for (int i = 0; i < Array.getLength(arr); ++i) {
            size += elemParser.write(dstBuf, (Message) Array.get(arr, i));
        }
        return size;
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        int size = Array.getLength(ReflectUtil.justGet(m, targetField));
        sizeField.set(m, size);
    }

    @Override
    public int getStaticSize() {
        return 0;
    }

}
