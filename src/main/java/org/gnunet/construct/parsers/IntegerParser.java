/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;
import org.gnunet.construct.ProtocolViolationException;
import org.gnunet.construct.ReflectUtil;

import java.lang.reflect.Field;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.List;

/**
 *
 * todo: error checking on numeric overflow
 */
public class IntegerParser implements Parser {

    public static final boolean UNSIGNED = false;
    public static final boolean SIGNED = true;

    private final int byteSize;

    private final boolean isSigned;

    private final ReflectUtil.NumField targetField;


    public IntegerParser(final int byteSize, final boolean isSigned,
                         final Field f) {
        this.byteSize = byteSize;
        this.isSigned = isSigned;

        targetField = new ReflectUtil.NumField(f);
    }

    @Override
    public int getSize(final Message srcObj) {
        return byteSize;
    }

    @Override
    public int parse(final ByteBuffer srcBuf, int frameOffset, Message frameObj, final Message dstObj, List<Field>
            frameSizePath) {
        try {
            if (targetField.isBig()) {
                targetField.set(dstObj, IntegerUtil.readBigInteger(srcBuf, isSigned, byteSize));
            } else {
                targetField.set(dstObj, IntegerUtil.readLong(srcBuf, isSigned, byteSize));
            }
        } catch (BufferUnderflowException e) {
            throw new ProtocolViolationException("Underflow while parsing " + targetField);
        }
        return byteSize;
    }

    @Override
    public int write(final ByteBuffer dstBuf, final Message srcObj) {
        if (targetField.isBig()) {
            IntegerUtil.writeBitInteger(targetField.getBig(srcObj), dstBuf, isSigned, byteSize);
        } else {
            // todo: error checking on numeric overflow, if requested
            IntegerUtil.writeLong(targetField.get(srcObj), dstBuf, isSigned, byteSize);
        }
        return byteSize;
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        // todo: optimize this!
        /*
        if (frameSizePath != null) {
            ReflectUtil.justSetInt(frameObj, frameSizePath, frameSize);
        }
        */
    }

    @Override
    public int getStaticSize() {
        return byteSize;
    }
}
