/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;


import java.math.BigInteger;
import java.nio.ByteBuffer;

public class IntegerUtil {
    public static long readLong(ByteBuffer srcBuf, boolean isSigned, int byteSize) {
        long val = 0;

        final int first = srcBuf.position();
        final int last = first + byteSize - 1;

        // read all bytes except the last
        while (srcBuf.position() != last) {
            byte b = srcBuf.get();
            // byte b may be signed, if so interpret it as unsigned byte; store it in an int
            int s = b >= 0 ? b : (256 + b);

            val |= s;
            val <<= 8;
        }

        // read the last byte, we don't have to shift val after that
        byte b = srcBuf.get();
        int s = b >= 0 ? b : (256 + b);
        val |= s;

        if (isSigned) {
            // explicitly OR signRaw bit to the right place if the source buffer is
            // too large
            long sign = (srcBuf.get(first) & 0x80);
            val |= (sign << 7);
        }

        return val;
    }

    public static void writeLong(final long val, final ByteBuffer dstBuf, boolean isSigned, int byteSize) {
        long myval = val;

        // position of the last byte we are responsible to write
        int last = dstBuf.position() + byteSize - 1;

        while (last >= dstBuf.position()) {
            dstBuf.put(last, (byte) (myval & 0xFF));
            myval >>>= 8;
            last -= 1;
        }

        if (isSigned) {
            // a long has 8 bytes, shift by 7 bytes (non-arithmetically) to get the signRaw
            byte sign = (byte) ((val >>> (7*8)) & 0x80);
            // remove the signRaw bit from the buffer
            dstBuf.put(dstBuf.position() + byteSize - 1, (byte) (dstBuf.get(dstBuf.position() + byteSize - 1) & ~sign));
            // ... and put it in the right place (lowest byte)
            dstBuf.put(dstBuf.position(), (byte) (dstBuf.get(dstBuf.position()) | sign));

        }

        dstBuf.position(dstBuf.position() + byteSize);
    }


    public static void writeBitInteger(BigInteger big, ByteBuffer dstBuf, boolean isSigned, int byteSize) {
        throw new UnsupportedOperationException("not yet implemented");
    }

    public static BigInteger readBigInteger(final ByteBuffer srcBuf, boolean isSigned, int byteSize) {
        throw new UnsupportedOperationException("not yet implemented");
    }
}
