/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;


import org.gnunet.construct.Message;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.List;

public class DoubleParser implements Parser {
    
    private final Field targetField;
    
    public DoubleParser(Field f) {
        targetField = f;
    }

    @Override
    public int getSize(Message srcObj) {
        return Double.SIZE / 8;
    }

    @Override
    public int parse(ByteBuffer srcBuf, int frameOffset, Message frameObj, Message dstObj, List<Field> frameSizePath) {
        double d = srcBuf.getDouble();
        try {
            targetField.setDouble(dstObj, d);
        } catch (IllegalAccessException e) {
            throw new AssertionError("cannot access field (should have been caught in Construct)");
        }
        return Double.SIZE / 8;
    }

    @Override
    public int write(ByteBuffer dstBuf, Message srcObj) {
        double d;
        try {
            d = targetField.getDouble(srcObj);
        } catch (IllegalAccessException e) {
            throw new AssertionError("field does not exist (should be caught in Construct)");
        }
        dstBuf.putDouble(d);
        return 8;
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        // nothing to do here
    }

    @Override
    public int getStaticSize() {
        return Double.SIZE / 8;
    }
}
