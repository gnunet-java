/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;
import org.gnunet.construct.ProtocolViolationException;
import org.gnunet.construct.ReflectUtil;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.List;

public class StringParser implements Parser {

    private final String cset;
    private final boolean optional;
    private final Field targetField;

    public StringParser(final String charset, boolean optional, final Field f) {
        this.targetField = f;
        this.optional = optional;
        this.cset = charset;
    }

    @Override
    public int getSize(final Message srcObj) {
        final String s = (String) ReflectUtil.justGet(srcObj, targetField);
        if (s == null) {
            if (optional) {
                return 0;
            } else {
                throw new AssertionError("non-optional string in field '" + targetField + "' cannot be null");
            }
        }
        try {
            final byte[] b = s.getBytes(cset);
            return b.length + 1;
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public int parse(final ByteBuffer srcBuf, int frameOffset, Message frameObj, final Message dstObj, List<Field>
            frameSizePath) {

        if (optional) {
            if (frameSizePath == null) {
                throw new AssertionError("optional string with no length field in the message!");
            }
            final int frameSize = ReflectUtil.justGetInt(frameObj, frameSizePath);
            int remaining = frameOffset + frameSize - srcBuf.position();

            if (remaining == 0) {
                if (!optional) {
                    throw new ProtocolViolationException("no data received for non-optional string");
                }
                ReflectUtil.justSet(dstObj, targetField, null);
                return 0;
            }
        }

        if (!srcBuf.hasRemaining()) {
            throw new ProtocolViolationException("no data for non-optional string field " +
                targetField);
        }

        int length = 0;

        while (srcBuf.get(srcBuf.position() + length) != 0) {
            length++;
        }

        final byte[] stringData = new byte[length];
        
        srcBuf.get(stringData);

        if (srcBuf.get() != 0) {
            throw new AssertionError("programming error");
        }

        String str;
        try {
            str = new String(stringData, cset);
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException();
        }

        ReflectUtil.justSet(dstObj, targetField, str);

        return length + 1;
    }

    @Override
    public int write(final ByteBuffer dstBuf, final Message srcObj) {
        String s = (String) ReflectUtil.justGet(srcObj, targetField);
        
        if (s == null) {
            if (!optional) {
                throw new AssertionError("non-optional string cannot be null");
            }
            return 0;
        }
        
        byte[] b;
        try {
            b = s.getBytes(cset);
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException();
        }

        dstBuf.put(b);
        dstBuf.put((byte) 0);

        // +1 for the 0-byte
        return b.length + 1;
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        if (frameSizePath != null) {
            ReflectUtil.justSetInt(frameObj, frameSizePath, frameSize);
        }
    }

    @Override
    public int getStaticSize() {
        return optional ? 0 : 1;
    }

}
