/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;
import org.gnunet.construct.ReflectUtil;
import org.gnunet.construct.StringTerminationType;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.List;

public class VariableSizeStringParser implements Parser {
    private final Field targetField;
    private ReflectUtil.NumField sizeField;
    private StringTerminationType terminationType;


    public VariableSizeStringParser(StringTerminationType terminationType,
                                    Field sizeField, Field arrayField) {
        targetField = arrayField;
        this.sizeField = new ReflectUtil.NumField(sizeField);
        this.terminationType = terminationType;
    }

    @Override
    public int getSize(final Message src) {
        final String str = (String) ReflectUtil.justGet(src, targetField);

        if (str == null) {
            throw new RuntimeException("string");
        }
        switch (terminationType) {
            case NONE:
                return str.length();
            case ZERO_EXCLUDED:
                return str.length() + 1;
            case ZERO_INCLUDED:
                return str.length() + 1;
            default:
                throw new AssertionError();
        }
    }

    @Override
    public int parse(final ByteBuffer srcBuf, int frameOffset, Message frameObj, final Message dstObj, List<Field>
            frameSizePath) {
        final int size = (int) sizeField.get(dstObj);
        final int strSize;
        final int skipSize;

        switch (terminationType) {
            case NONE:
                strSize = size;
                skipSize = 0;
                break;
            case ZERO_EXCLUDED:
                strSize = size;
                skipSize = 1;
                break;
            case ZERO_INCLUDED:
                strSize = size-1;
                skipSize = 1;
                break;
            default:
                throw new AssertionError();
        }

        byte[] data = new byte[strSize];
        srcBuf.get(data);
        srcBuf.position(srcBuf.position() + skipSize);
        ReflectUtil.justSet(dstObj, targetField, new String(data));
        return size;
    }

    @Override
    public int write(final ByteBuffer dstBuf, final Message src) {
        int n;
        final String str = (String) ReflectUtil.justGet(src, targetField);

        if (str == null) {
            throw new RuntimeException("string must not be null");
        }

        byte[] data = str.getBytes();

        switch (terminationType) {
            case NONE:
                dstBuf.put(data);
                n = data.length;
                break;
            case ZERO_INCLUDED:
                dstBuf.put(data);
                dstBuf.put((byte) 0);
                n = data.length + 1;
                break;
            case ZERO_EXCLUDED:
                dstBuf.put(data);
                dstBuf.put((byte) 0);
                n = data.length + 1;
                break;
            default:
                throw new AssertionError();
        }
        return n;
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        int size;
        final String str = (String) ReflectUtil.justGet(m, targetField);

        if (str == null) {
            throw new RuntimeException("string must not be null");
        }

        if (terminationType == StringTerminationType.ZERO_INCLUDED) {
            size = str.length() + 1;
        } else {
            size = str.length();
        }
        sizeField.set(m, size);
    }

    @Override
    public int getStaticSize() {
        return 0;
    }

}
