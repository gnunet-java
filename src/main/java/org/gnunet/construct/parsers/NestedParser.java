/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;
import org.gnunet.construct.ProtocolViolationException;
import org.gnunet.construct.ReflectUtil;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.List;


public class NestedParser implements Parser {
    private final Field targetField;

    private final Parser nestedParser;
    private boolean newFrame;

    boolean optional;

    public NestedParser(final Parser p, boolean optional, final Field f, boolean newFrame) {
        targetField = f;
        this.optional = optional;
        this.nestedParser = p;
        this.newFrame = newFrame;
    }

    @Override
    public int getSize(final Message src) {
        Message inner = (Message) ReflectUtil.justGet(src, targetField);
        if (inner == null) {
            if (optional)
                return 0;
            throw new AssertionError(String.format("empty non-optional nested message in field '%s'", targetField));
        }
        return nestedParser.getSize(inner);
    }

    @Override
    public int parse(final ByteBuffer srcBuf, int frameOffset, Message frameObj, final Message dstObj, List<Field>
            frameSizePath) {
        if (newFrame) {
            frameObj = dstObj;
            frameOffset = 0;
        }

        if (optional) {
            if (frameSizePath == null) {
                throw new AssertionError("optional nested message needs @FrameSize");
            }

            int remaining = frameOffset + ReflectUtil.justGetInt(frameObj, frameSizePath) - srcBuf.position();
            if (remaining < 0) {
                throw new ProtocolViolationException("remaining size negative");
            }
            if (remaining == 0) {
                if (!optional) {
                    throw new ProtocolViolationException("not optional");
                }
                ReflectUtil.justSet(dstObj, targetField, null);
                return 0;
            }
        }
        if (targetField.getType().isInterface()) {
            throw new AssertionError(String.format("Target field '%s' is an interface, not a class.", targetField));
        }
        ReflectUtil.justSet(dstObj, targetField, ReflectUtil.justInstantiate(targetField.getType()));

        try {
            return nestedParser.parse(srcBuf, frameOffset,
                 frameObj, (Message) ReflectUtil.justGet(dstObj, targetField), frameSizePath);
        } catch (ProtocolViolationException e) {
            throw e.augmentPath("nested parser on " + targetField.toString());
        }
    }

    @Override
    public int write(final ByteBuffer dstBuf, final Message src) {
        Object nestedMessage = ReflectUtil.justGet(src, targetField);
        if (nestedMessage == null) {
            return 0;
        }
        return nestedParser.write(dstBuf, (Message) nestedMessage);
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        Message nestedMessage = (Message) ReflectUtil.justGet(m, targetField);

        if (newFrame) {
            nestedParser.patch(nestedMessage, nestedParser.getSize(nestedMessage), null, nestedMessage);
        } else {
            nestedParser.patch(nestedMessage, frameSize, frameSizePath, frameObj);
        }
    }

    @Override
    public int getStaticSize() {
        return nestedParser.getStaticSize();
    }

}
