/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;
import org.gnunet.construct.ProtocolViolationException;
import org.gnunet.construct.ReflectUtil;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

/**
 * A Sequence of Parsers that operate on the same object.
 * @author Florian Dold
 *
 */
public class SequenceParser implements Parser {

    private final List<Parser> childParsers = new LinkedList<Parser>();
    private List<Field> myFrameSizePath;

    public SequenceParser() {
    }

    public void add(final Parser p) {
        childParsers.add(p);
    }

    @Override
    public int getSize(final Message src) {
        int size = 0;
        for (final Parser p : childParsers) {
            size += p.getSize(src);
        }
        return size;
    }

    @Override
    public int parse(final ByteBuffer srcBuf, int frameOffset,
                     Message frameObj, final Message dst, List<Field> frameSizePath) {
        int size = 0;
        for (final Parser p : childParsers) {
            try {
                size += p.parse(srcBuf, frameOffset, frameObj, dst,
                        frameSizePath == null ? myFrameSizePath : frameSizePath);
            } catch (ProtocolViolationException e) {
                throw e.augmentPath("(sequence parser)");
            }
        }
        return size;
    }

    @Override
    public int write(final ByteBuffer dstBuf, final Message src) {
        int size = 0;
        for (final Parser p : childParsers) {
            size += p.write(dstBuf, src);
        }
        return size;
    }

    @Override
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj) {
        // todo: this should be optimized / only be done by the topmost sequence parse => introduce a boolean parameter
        if (myFrameSizePath != null) {
            ReflectUtil.justSetInt(frameObj, myFrameSizePath, frameSize);
        }

        for (final Parser p : childParsers) {
            p.patch(m, frameSize, frameSizePath == null ? myFrameSizePath : frameSizePath, frameObj);
        }
    }

    @Override
    public int getStaticSize() {
        int accum = 0;
        for (Parser p : childParsers) {
            accum += p.getStaticSize();
        }
        return accum;
    }

    public void setFrameSizePath(List<Field> frameSizePath) {
        this.myFrameSizePath = frameSizePath;
    }
}
