/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct.parsers;

import org.gnunet.construct.Message;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.List;


public interface Parser {
    /**
     * Compute the exact size of the object's binary representation in bytes.
     * 
     * @param srcObj a message object with all fields filled out appropriately
     * @return the exact size of the object's binary representation in bytes
     */
    public int getSize(Message srcObj);


    /**
     * Parse from a ByteBuffer into a destination object.
     *
     * @param srcBuf the buffer containing the binary data to construct this object
     * @param frameStart start of the current frame, relative to the beginning of srcBuf
     * @param frameObj the object containing the dstObj, dstObj if dstObj itself is the frame object
     * @param dstObj the object whose members are written according according to the data in srcBuf
     * @param frameSizePath
     * @return number of byres read from srcBuf
     */
    public int parse(ByteBuffer srcBuf, int frameStart, Message frameObj, Message dstObj, List<Field> frameSizePath);

    /**
     * 
     * @param dstBuf destination buffer for the binary representation of the object
     * @param srcObj object to serialize to binary form
     * @return number of bytes written to buf (todo: we are using a ByteBuffer now, this is obsolete)
     */
    public int write(ByteBuffer dstBuf, Message srcObj);

    /**
     * Parser-dependent method; sets members of the Message m (or Messages nested in m) which are
     * values inferable by the parser.
     * Examples: Union tags, size fields.
     *
     * @param m the message object to patch
     * @param frameSize the size of the containing message
     * @param frameSizePath
     * @param frameObj the object containing the message (and possibly size fields)
     */
    public void patch(Message m, int frameSize, List<Field> frameSizePath, Message frameObj);

    /**
     * Return a lower bound for the size of the message in bytes
     *
     * @return minimum static size of the message in bytes
     */
    int getStaticSize();
}
