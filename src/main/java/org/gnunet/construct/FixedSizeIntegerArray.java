package org.gnunet.construct;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * An array of integers with static size.
 *
 * @author Florian Dold
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FixedSizeIntegerArray {
    int length();
    int bitSize();
    boolean signed();
}
