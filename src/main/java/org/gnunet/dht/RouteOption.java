/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.dht;

/**
 * Options passed to the dht service for routing requests.
 */
enum RouteOption {
    /**
     * Default.  Do nothing special.
     */
    NONE(0),
    /**
     * Each peer along the way should look at 'enc' (otherwise
     * only the k-peers closest to the key should look at it).
     */
    DEMULTIPLEX_EVERYWHERE(1),
    /**
     * We should keep track of the route that the message
     * took in the P2P network.
     */
    RECORD_ROUTE(2),
    /**
     * This is a 'FIND-PEER' getRequestIdentifier, so approximate results are fine.
     */
    FIND_PEER(4),
    /**
     * Possible message option for query key randomization.
     */
    BART(8);

    int val;

    RouteOption(int val) {
        this.val = val;
    }
}
