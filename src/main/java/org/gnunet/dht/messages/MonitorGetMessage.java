/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.dht.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.VariableSizeArray;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.HashCode;
import org.gnunet.util.PeerIdentity;

/**
 * Message to monitor get requests going through peer, DHT service -> clients.
 */
@UnionCase(149)
public class MonitorGetMessage implements GnunetMessage.Body {
    /**
     * Message options, actually an 'enum GNUNET_DHT_RouteOption' value.
     */
    @UInt32
    public int options;

    /**
     * The type of data in the getRequestIdentifier.
     */
    @UInt32
    public int type;

    /**
     * Hop count
     */
    @UInt32
    public int hopCount;

    /**
     * Replication level for this message
     */
    @UInt32
    public int desiredReplicationLevel;

    /**
     * Number of peers recorded in the outgoing path from source to the
     * storage location of this message.
     */
    @UInt32
    public int getPathLength;

    /**
     * The key to store the value under.
     */
    @NestedMessage
    public HashCode key;

    @VariableSizeArray(lengthField = "getPathLength")
    public PeerIdentity[] getPath;
}
