/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.dht.messages;

import org.gnunet.construct.*;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.HashCode;
import org.gnunet.util.PeerIdentity;

@UnionCase(145)
public class ClientResultMessage implements GnunetMessage.Body {
    @UInt32
    public int type;
    @UInt32
    public int putPathLength;
    @UInt32
    public int getPathLength;
    @UInt64
    public long uid;
    @NestedMessage
    public AbsoluteTimeMessage expiration;
    @NestedMessage
    public HashCode key;
    @VariableSizeArray(lengthField = "putPathLength")
    public PeerIdentity[] putPath;
    @VariableSizeArray(lengthField = "getPathLength")
    public PeerIdentity[] getPath;
    @FillWith @UInt8
    public byte[] data;
}
