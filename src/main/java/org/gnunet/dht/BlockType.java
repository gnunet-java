/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.dht;

/**
 * Information on how to interpret a block of data.
 */
public enum BlockType {
    /**
     * Any type of block, used as a wildcard when searching.  Should
     * never be attached to a specific block.
     */
    ANY(0),
    /**
     * Data block (leaf) in the CHK tree.
     */
    DBLOCK(1),
    /**
     * Inner block in the CHK tree.
     */
    IBLOCK(2),
    /**
     * Type of a block representing a keyword search result.  Note that
     * the values for KBLOCK, SBLOCK and NBLOCK must be consecutive.
     */
    KBLOCK(3),
    /**
     * Type of a block that is used to advertise content in a namespace.
     */
    SBLOCK(4),
    /**
     * Type of a block that is used to advertise a namespace.
     */
    NBLOCK(5),
    /**
     * Type of a block representing a block to be encoded on demand from disk.
     * Should never appear on the network directly.
     */
    FS_ONDEMAND(6),
    /**
     * Type of a block that contains a HELLO for a peer (for
     * DHT find-peer operations).
     */
    DHT_HELLO(7),
    /**
     * Block for testing.
     */
    TEST(8),
    /**
     * Block for storing .gnunet-domains
     */
    DNS(10),
    /**
     * Block for storing record data
     */
    NAMERECORD(11);

    public final int val;

    BlockType(int val) {
        this.val = val;
    }
}
