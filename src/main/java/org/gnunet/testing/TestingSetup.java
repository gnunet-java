/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.testing;

import org.gnunet.util.Program;

/**
 * A testing setup is responsible for configuring the loggers during testing, and can
 * start gnunet subsystems (like statistics, core, etc.).
 *
 * @author Florian Dold
 */
public final class TestingSetup {

    private TestingSetup() {

    }

    public static class SetupException extends RuntimeException {
        public SetupException(Exception e) {
            super(e);
        }
        public SetupException(String msg) {
            super(msg);
        }

    }

    public static void setup() {
        String log = System.getenv("GNJ_LOGLEVEL");
        if (log != null) {
            Program.configureLogging(log, null);
        } else {
            Program.configureLogging("WARN", null);
        }
    }
}
