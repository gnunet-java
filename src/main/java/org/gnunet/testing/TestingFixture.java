package org.gnunet.testing;

import org.gnunet.util.Scheduler;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.Timeout;

/**
 * Default JUnit4 fixture methods for gnunet-java tests.
 * Resets the scheduler properly.
 */
public class TestingFixture {

    /**
     * All gnunet-java tests that inherit from the fixture
     * have a 10-second default timeout.
     * FIXME: for a longer timeout, which has precedence?
     */
    @Rule
    public Timeout defaultTimeout = new Timeout(10000);

    @Before
    public void beginGNJTest() {
        Scheduler.forceReset();
    }

    @After
    public void endGNJTest() {
        Scheduler.forceReset();
    }
}
