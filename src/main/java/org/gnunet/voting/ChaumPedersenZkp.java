/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.voting;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.Message;
import org.gnunet.secretsharing.Parameters;

import java.math.BigInteger;

/**
 * Proof in zero knowledge of dlog equality.
 * Proves log_g(x) = log_h(y) = alpha
 * FIXME: get the details right, the Cramers voting paper uses a proof that is a
 * FIXME: bit different from the plain chaum pedersen proof
 */
public class ChaumPedersenZkp implements Message {
    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] commit_a;
    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] commit_b;

    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] challenge_d;

    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] response_r;


    /**
     * Verify the simulated proof.  That is, don't check
     * if the challenge was actually computed correctly from the commits.
     */
    public boolean verifySim(BigInteger x, BigInteger y, BigInteger coeffG, BigInteger h) {
        BigInteger a = new BigInteger(1, commit_a);
        BigInteger b = new BigInteger(1, commit_b);
        BigInteger d = new BigInteger(1, challenge_d);
        BigInteger r = new BigInteger(1, response_r);
        BigInteger g = Parameters.elgamalG;
        BigInteger p = Parameters.elgamalP;

        if (!b.equals(h.modPow(r, p).multiply(y.multiply(coeffG.modInverse(p)).modPow(d, p)).mod(p))) {
            System.out.println("b not equal");
            return false;
        }

        if (!a.equals(g.modPow(r, p).multiply(x.modPow(d, p)).mod(p))) {
            System.out.println("a not equal");
            return false;
        }


        return true;
    }
}


