package org.gnunet.voting;

import org.gnunet.construct.Message;
import org.gnunet.util.crypto.EcdsaSignedMessage;

public class SignedEncryptedVote implements Message {
    EcdsaSignedMessage<EncryptedVote> signedMessage;
}
