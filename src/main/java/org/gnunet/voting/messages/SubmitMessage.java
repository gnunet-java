package org.gnunet.voting.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.HashCode;
import org.gnunet.util.crypto.EcdsaPublicKey;
import org.gnunet.util.crypto.EcdsaSignature;
import org.gnunet.voting.EncryptedVote;

/**
 * Message send by the voter to the election authority to submit a vote.
 */
@UnionCase(42007)
public class SubmitMessage implements GnunetMessage.Body {
    /**
     * Identifier of the ballot we want to vote in.
     */
    @NestedMessage
    public HashCode ballotGuid;
    /**
     * Public key of the voter.
     */
    @NestedMessage
    public EcdsaPublicKey voterPub;
    /**
     * Group certificate of the voter.
     */
    @NestedMessage
    public EcdsaSignature groupCert;
    /**
     * Expiration time of the group certificate, checked by the authority.
     */
    @NestedMessage
    public AbsoluteTimeMessage groupCertExpiration;

    /**
     * The encrypted vote, including zero knowledge proofs
     * for correctness.
     */
    @NestedMessage
    public EncryptedVote encryptedVote;
}
