package org.gnunet.voting.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.crypto.EddsaSignature;


/**
 * Response from the authorities to the issuer.
 */
@UnionCase(42012)
public class BallotRegisterSuccessMessage implements GnunetMessage.Body {
    @NestedMessage
    public EddsaSignature registrationSignature;
}
