package org.gnunet.voting.messages;

import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;

@UnionCase(42015)
public class KeyQueryFailureMessage implements GnunetMessage.Body {
    @ZeroTerminatedString
    public String reason;
}
