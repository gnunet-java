/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.voting.messages;

import org.gnunet.construct.Message;
import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.secretsharing.ThresholdPublicKey;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.HashCode;
import org.gnunet.util.crypto.EddsaSignature;

@UnionCase(42014)
public class KeyQueryResponseMessage implements GnunetMessage.Body {

    public static class BallotPublicKey implements Message {
        @NestedMessage
        public HashCode ballotGuid;

        @NestedMessage
        public ThresholdPublicKey publicKey;
    }

    @NestedMessage
    public EddsaSignature signature;

    @UInt32
    public int purpose;

    @NestedMessage
    public BallotPublicKey signedGuidKey;
}
