package org.gnunet.voting.messages;


import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.crypto.EddsaSignature;

@UnionCase(42008)
public class SubmitSuccessMessage implements GnunetMessage.Body {
    @NestedMessage
    public EddsaSignature confirmationSig;
}
