package org.gnunet.voting.messages;


import org.gnunet.construct.IntegerFill;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

@UnionCase(42006)
public class ResultQueryResponseMessage implements GnunetMessage.Body {
    @IntegerFill(signed = false, bitSize = 32)
    public long[] results;
}
