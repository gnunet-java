package org.gnunet.voting.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.HashCode;

@UnionCase(42005)
public class ResultQueryMessage implements GnunetMessage.Body {
    @NestedMessage
    public HashCode ballotGuid;
}
