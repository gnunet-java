package org.gnunet.voting.messages;

import org.gnunet.construct.IntegerFill;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * Sent by an issuer to the authorities to register a ballot with them.
 */
@UnionCase(42001)
public class BallotRegisterRequestMessage implements GnunetMessage.Body {
    @IntegerFill(signed = false, bitSize = 8)
    public byte[] compressedBallotConfig;
}
