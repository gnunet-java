/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.voting;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.Message;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.VariableSizeArray;
import org.gnunet.secretsharing.Parameters;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Disjunction of Chaum Pedersen ZKPs.
 */
public class DisjunctionZkp implements Message {
    @UInt64
    public int numProofs;
    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] challenge_c;
    @VariableSizeArray(lengthField = "numProofs")
    public ChaumPedersenZkp[] chaumPedersenZkps;

    public boolean verifyChallenge() {
        BigInteger c_actual = new BigInteger(1, challenge_c);
        BigInteger c_expected = BigInteger.ZERO;
        for (ChaumPedersenZkp chaumPedersenZkp : chaumPedersenZkps) {
            BigInteger d = new BigInteger(1, chaumPedersenZkp.challenge_d);
            c_expected = c_expected.add(d).mod(Parameters.elgamalQ);
        }
        return c_actual.equals(c_expected) && computeChallengeFromCommits().equals(c_actual);
    }

    public BigInteger computeChallengeFromCommits() {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("crypto algorithm 'SHA-512' required but not provided");
        }
        for (ChaumPedersenZkp chaumPedersenZkp : chaumPedersenZkps) {
            digest.update(chaumPedersenZkp.commit_a);
            digest.update(chaumPedersenZkp.commit_b);
        }
        return (new BigInteger(digest.digest())).mod(Parameters.elgamalQ);
    }
}
