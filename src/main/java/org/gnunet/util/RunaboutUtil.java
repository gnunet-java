/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import org.gnunet.construct.MessageLoader;
import org.grothoff.Runabout;

import java.lang.reflect.Method;
import java.util.ArrayList;


/**
 * Utility methods for the runabout.
 */
public class RunaboutUtil {
    public static ArrayList<Class> getRunaboutVisitees(Runabout r) {
        Class rc = r.getClass();
        ArrayList<Class> ret = new ArrayList<Class>(5);
        for (Method m : rc.getMethods()) {
            if (!(m.getName().equals("visit") && m.getParameterTypes().length == 1)) {
                continue;
            }
            ret.add(m.getParameterTypes()[0]);
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    public static int[] getRunaboutMessageTypes(Runabout r) {
        ArrayList<Class> visitees = getRunaboutVisitees(r);
        int[] msgtypes = new int[visitees.size()];
        for (int i = 0; i < visitees.size(); ++i) {
            msgtypes[i] = MessageLoader.getUnionTag(GnunetMessage.Body.class, visitees.get(i));
        }
        return msgtypes;
    }
}
