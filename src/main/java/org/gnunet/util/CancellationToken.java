/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import java.util.LinkedList;
import java.util.List;

/**
 * Token that calls a method or other cancelables on cancellation,
 * and stores its cancellation state.
 */
public class CancellationToken implements Cancelable {
    private boolean isCanceled;
    private List<Cancelable> cancelables = new LinkedList<Cancelable>();
    @Override
    public void cancel() {
        if (isCanceled) {
            throw new AssertionError();
        }
        for (Cancelable c : cancelables) {
            c.cancel();
        }
    }

    public void add(Cancelable cancelable) {
        if (isCanceled) {
            throw new AssertionError();
        }
        cancelables.add(cancelable);
    }
}
