/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;


import org.gnunet.construct.*;


/**
 * Every message used to communicate between gnunet components uses this format.
 * First, a header is sent, containing the size of the overall message (including the header), as
 * well as the type of the message. After that the message body is sent, whose format is specified
 * by the message type.
 *
 */
public final class GnunetMessage implements Message {
    public static final int MINIMAL_SIZE = Header.SIZE;


    /**
     * The header of every gnunet message.
     */
    public static final class Header implements Message {
        public static final int SIZE = 4;

        @FrameSize
        @UInt16
        public int messageSize;

        @UInt16
        public int messageType;
    }

    /**
     * The common interface for every message body.
     *
     */
    public static interface Body extends MessageUnion {
    }


    /**
     * Create a GnunetMessage from its body only. The header is added and filled with the relevant information
     * automatically.
     *
     * @param b the message body to convert
     * @return a complete and valid gnunet message
     */
    public static GnunetMessage fromBody(Body b) {
        GnunetMessage msg = new GnunetMessage();
        msg.header = new Header();
        msg.header.messageSize = Header.SIZE + Construct.getSize(b);
        msg.header.messageType = MessageLoader.getUnionTag(GnunetMessage.Body.class, b.getClass());
        msg.body = b;
        return msg;
    }

    @NestedMessage
    public Header header;

    @Union(tag = "header.messageType")
    public Body body;
}
