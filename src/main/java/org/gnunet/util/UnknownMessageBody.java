/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

/**
 * Special GnunetMessage body, used to signal that the message containing the body
 * is not understood, and therefore no real message body could be constructed.
 *
 * Note that this class implements GnunetMessage.Body but does not have a MessageID associated.
 * This message should not, and can not, be sent/received over the network directly as a message body.
 *
 * @author Florian Dold
 */
public class UnknownMessageBody implements GnunetMessage.Body {
    public int id;
    public byte[] data;
}
