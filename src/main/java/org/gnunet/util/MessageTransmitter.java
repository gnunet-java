/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;


/**
 * Callback object for transmitting messages.
 */
public interface MessageTransmitter {
    /**
     * Called when the client is ready to transmit messages, or on timeout/error.
     *
     * @param sink A message sink that receives messages to be transmitted by the client,
     *             or null on timeout/error.
     */
    public void transmit(Connection.MessageSink sink);


    /**
     * Called when the transmit getRequestIdentifier could not be fullfilled.
     *
     * After transmit has been called, handleError will not be called anymore (until the next transmit getRequestIdentifier)
     */
    void handleError();
}
