/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;


import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.Message;

import java.util.Arrays;


/**
 * Identity of a peer.  Wrapper for an
 * EdDSA key.
 */
public class PeerIdentity implements Message {

    @FixedSizeIntegerArray(length = 32, signed = false, bitSize = 8)
    public byte[] data;

    /**
     * Creates a zero-filled peer identity
     */
    public PeerIdentity() {
        data = new byte[32];
    }

    public String toString() {
        return Strings.dataToString(data);
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof PeerIdentity && Arrays.equals(((PeerIdentity) obj).data, this.data);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }

    public static PeerIdentity fromString(String value) {
        PeerIdentity p = new PeerIdentity();
        p.data = Strings.stringToData(value, p.data.length);
        return (p.data == null) ? null : p;
    }
}

