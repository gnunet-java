/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import org.gnunet.construct.Message;
import org.gnunet.construct.UInt64;


/**
 * Representation of a RelativeTime object, to be sent over the network.
 */
public class RelativeTimeMessage implements Message {

    /**
     * Value__ still in Java-byte order, needs to be converted to Network byte
     * order by the Construct class.
     */
    @UInt64
    public long value__;

    public RelativeTimeMessage(final long value) {
        this.value__ = value;
    }

    public RelativeTimeMessage() {
        // default constructor needed for Construct
    }

    public RelativeTimeMessage(final RelativeTime t) {
        if (t.equals(RelativeTime.FOREVER)) {
            this.value__ = -1;
        } else {
            this.value__ = t.getMicroseconds();
        }
    }

}
