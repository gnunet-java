/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import org.gnunet.construct.ProtocolViolationException;
import org.grothoff.Runabout;

/**
 * An abstract base class for message receivers that want to use the runabout, dispatches
 * messages to the appropriate visit method.
 */
public abstract class RunaboutMessageReceiver extends Runabout implements MessageReceiver  {
    public void process(GnunetMessage.Body msg) {
        this.visitAppropriate(msg);
    }
    public void handleUnknownMessageType(UnknownMessageBody unknownMessageBody) {
        throw new ProtocolViolationException(
                String.format("unexpected message type: %s", unknownMessageBody.id));
    }
    public void visit(UnknownMessageBody unknownMessageBody) {
        // FIXME: UnknownMessageBody is ugly and should be removed entirely
        handleUnknownMessageType(unknownMessageBody);
    }
}
