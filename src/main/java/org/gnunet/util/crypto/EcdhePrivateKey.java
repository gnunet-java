/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util.crypto;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.Message;
import org.gnunet.util.HashCode;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Private key for elliptic curve Diffie Hellman exchange.
 * Note that (in contrast to NaCl) the key is encoded
 * as big endian.
 */
public class EcdhePrivateKey implements Message {
    /**
     * Private key byte string, in big endian form.
     */
    @FixedSizeIntegerArray(bitSize = 8, signed = false, length = 32)
    public byte[] d;

    /**
     * Create an ecdh private key without allocating the key data.
     */
    public EcdhePrivateKey() {
        // empty constructor for org.gnunet.construct.*
    }

    /**
     * Get the public key for this private key.
     *
     * @return the public key for this private key
     */
    public EcdhePublicKey getPublicKey() {
        BigInteger dCoeff = this.asCoefficient();
        Ed25519 A = Ed25519.B.scalarmult(dCoeff);
        return new EcdhePublicKey(A);
    }

    /*
     * Derive key material from the give public key and a this private ECDHE key.
     *
     * @param publicKey public key to use for the ECDH
     * @return key material
     */
    public HashCode ecdh(EcdhePublicKey publicKey) {
        BigInteger dCoeff = this.asCoefficient();
        Ed25519 Q = publicKey.asPoint().scalarmult(dCoeff);
        // hash big endian representation of the x-coordinate.
        return HashCode.hash(Q.P0.toByteArray());
    }

    /**
     * Create a random private key.
     *
     * @return a random private key
     */
    public static EcdhePrivateKey createRandom() {
        SecureRandom sr = new SecureRandom();
        EcdhePrivateKey privateKey = new EcdhePrivateKey();
        privateKey.d = new byte[32];
        sr.nextBytes(privateKey.d);
        // clear bits so that d mod 8 = 0
        privateKey.d[31] &= (byte) 248;
        // make sure the key fits in 255 bits
        privateKey.d[0] &= (byte) 127;
        // make sure key does not have lots of leading zeros
        privateKey.d[0] |= (byte) 64;
        return privateKey;
    }

    public BigInteger asCoefficient() {
        /* We have a big endian value here ... */
        return new BigInteger(1, d);
    }


}
