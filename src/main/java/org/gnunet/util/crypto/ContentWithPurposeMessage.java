package org.gnunet.util.crypto;

import org.gnunet.construct.Message;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.Union;

/**
 * Purpose header fields, together with content union.
 */
public class ContentWithPurposeMessage<M extends SignedContentMessage> implements Message {
    @UInt32
    public int size;
    @UInt32
    public int purpose;
    @Union(tag = "purpose")
    public M m;
}
