/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util.crypto;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.Message;
import org.gnunet.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.util.Arrays;

public class EcdsaPublicKey implements Message {
    private static final Logger logger = LoggerFactory
            .getLogger(EcdsaPublicKey.class);

    /**
     * y-coordinate of the point on the curve.
     * The number is stored as little endian.
     */
    @FixedSizeIntegerArray(bitSize = 8, signed = false, length = 32)
    public byte[] y;

    /**
     * Create an ECDSA public key from a curve point.
     *
     * @param a curve point.
     */
    public EcdsaPublicKey(Ed25519 a) {
        y = a.encode();
    }

    /**
     * Create a public key without allocating space for the key data.
     * Necessary for constructing this class with org.gnunet.construct.*
     */
    public EcdsaPublicKey() {
        // empty
    }

    /**
     * Load an ECDSA key from a string.
     *
     * @param s string with the key data
     * @return a public key, or NULL if 's' not a valid public key encoding
     */
    public static EcdsaPublicKey fromString(String s) {
        EcdsaPublicKey publicKey = new EcdsaPublicKey();
        publicKey.y = new byte[32];
        if (Strings.stringToData(s, publicKey.y))
            return publicKey;
        return null;
    }

    /**
     * Create a random public key.  The generated key might not even be valid
     * (i.e. not on the curve), thus this method should only be used for testing.
     *
     * @return a random, possibly invalid public key
     */
    public static EcdsaPublicKey random() {
        SecureRandom sr = new SecureRandom();
        EcdsaPublicKey publicKey = new EcdsaPublicKey();
        sr.nextBytes(publicKey.y);
        return publicKey;
    }

    @Override
    public String toString() {
        return Strings.dataToString(y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EcdsaPublicKey publicKey = (EcdsaPublicKey) o;

        if (!Arrays.equals(y, publicKey.y)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(y);
        return result;
    }

    /**
     * Convert this public key to a point on the Ed25519 curve.
     *
     * @return a point corresponding to this key
     */
    public Ed25519 asPoint() {
        return Ed25519.decode(y);
    }
}
