/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util.crypto;

import org.gnunet.construct.Construct;
import org.gnunet.construct.Message;
import org.gnunet.construct.NestedMessage;

/**
 * A message together with a signature on the message and its purpose.
 */
public class EddsaSignedMessage implements Message {
    @NestedMessage
    public EddsaSignature signature;

    @NestedMessage(newFrame = true)
    public ContentWithPurposeMessage cpm;


    public EddsaSignedMessage() {
        // empty constructor required by org.gnunet.construct
    }

    public boolean verify(EddsaPublicKey signerPublicKey, Class<? extends SignedContentMessage> expectedClass) {
        if (!expectedClass.isInstance(cpm.m)) {
            return false;
        }
        byte[] data = Construct.toBinary(cpm);
        return signature.verifyRaw(data, signerPublicKey);
    }

    public EddsaSignedMessage(SignedContentMessage message, EddsaPrivateKey privateKey,
                              EddsaPublicKey publicKey) {
        cpm = new ContentWithPurposeMessage();
        cpm.m = message;
        Construct.patch(cpm);
        byte[] data = Construct.toBinary(cpm);
        signature = privateKey.signRaw(publicKey, data);
    }

    public EddsaSignedMessage(SignedContentMessage message, EddsaPrivateKey privateKey) {
        this(message, privateKey, privateKey.getPublicKey());
    }

}
