package org.gnunet.util.crypto;


import org.gnunet.construct.MessageUnion;

public interface SignedContentMessage extends MessageUnion {
    // empty, this is a tag interface
}
