/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util.crypto;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.util.Strings;

/**
 * Public key for elliptic curve diffie hellman exchange.
 */
public class EcdhePublicKey {
    /**
     * Point on the curve, in Ed25519-compressed form.
     */
    @FixedSizeIntegerArray(bitSize = 8, signed = false, length = 32)
    public byte[] y;

    /**
     * Create a public key without allocating the key's data.
     * Necessary for constructing the key with 'org.gnunet.construct.*'.
     */
    public EcdhePublicKey() {
        // empty
    }

    /**
     * Create a public key from the given point on the Ed25519 curve.
     *
     * @param a point to create the public key from
     */
    public EcdhePublicKey(Ed25519 a) {
        y = a.encode();
    }

    /**
     * Convert the public key to a point on the Ed25519 curve.
     *
     * @return point corresponding to this public key
     */
    public Ed25519 asPoint() {
        return Ed25519.decode(y);
    }

    /**
     * Get a GNUnet-style string representation of this key.
     *
     * @return the GNUnet-style string representation of this key.
     */
    @Override
    public String toString() {
        return Strings.dataToString(y);
    }

    /**
     * Load an ECDHE key from a string.
     *
     * @param s string with the key data
     * @return a public key
     */
    public EcdhePublicKey fromString(String s) {
        EcdhePublicKey publicKey = new EcdhePublicKey();
        Strings.stringToData(s, publicKey.y);
        return publicKey;
    }
}
