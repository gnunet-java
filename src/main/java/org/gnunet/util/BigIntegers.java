/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import com.google.common.base.Preconditions;

import java.math.BigInteger;

/**
 * Helper class for BigIntegers.
 */
public class BigIntegers {

    /**
     * Serialize a BigInteger, but do not add an extra bit for a
     * signRaw.
     *
     * @param bigInteger big integer to serialize
     * @param bits how many bits should the binary representation have?
     *             rounded up to the next multiple of 8.
     * @return big endian representation of the given BigInteger, without a signRaw bit
     */
    public static byte[] serializeUnsigned(BigInteger bigInteger, int bits) {
        byte[] bytes = bigInteger.toByteArray();
        int start;
        Preconditions.checkArgument(bigInteger.bitCount() <= bits);
        // skip byte that was only added to fit the signRaw
        if (bytes[0] == 0) {
            start = 1;
        } else {
            start = 0;
        }
        byte[] fixedBytes = new byte[(bits + 7) / 8];
        System.arraycopy(bytes, start, fixedBytes,
                fixedBytes.length - bytes.length + start, bytes.length - start);
        return fixedBytes;
    }
}
