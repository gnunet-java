/*
     This file is part of GNUnet.
     Copyright (C) 2009 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

/**
 * Callback object for receiving messages.
 * 
 */
public interface MessageReceiver {

    /**
     * Called when a message is received
     * 
     * @param msg   message received, null on deadline or fatal error
     */
    public void process(GnunetMessage.Body msg);


    /**
     * Called when an error (timeout, loss of connection) occured before receiving the message.
     */
    public void handleError();
}
