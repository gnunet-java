package org.gnunet.mq;


public interface NotifySentHandler {
    void onSent();
}
