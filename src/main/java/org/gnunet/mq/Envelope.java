package org.gnunet.mq;

import org.gnunet.util.Cancelable;
import org.gnunet.util.GnunetMessage;

/**
 * Container for a message to be sent by a message queue.
 */
public class Envelope implements Cancelable {
    public final GnunetMessage.Body message;
    private MessageQueue parentQueue;
    private NotifySentHandler notifySentHandler;

    public Envelope(GnunetMessage.Body message) {
        this.message = message;
    }

    public void notifySent(NotifySentHandler h) {
        this.notifySentHandler = h;
    }

    public void injectSent() {
        if (notifySentHandler != null)
            notifySentHandler.onSent();
    }

    public void cancel() {
        if (parentQueue == null)
            throw new AssertionError("can not onCancel an unqueued message");
    }

    void invokeSentNotification() {
        if (null != notifySentHandler)
            notifySentHandler.onSent();
    }
}
