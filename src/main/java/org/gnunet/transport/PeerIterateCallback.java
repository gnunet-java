package org.gnunet.transport;

import org.gnunet.util.PeerIdentity;

/**
 * ...
 *
 * @author Florian Dold
 */
public interface PeerIterateCallback {
    void processPeerAddress(PeerIdentity peer, Object hello);
}
