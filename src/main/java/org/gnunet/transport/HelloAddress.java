/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.transport;

import org.gnunet.util.PeerIdentity;


/**
 * An address for communicating with a peer.
 */
public class HelloAddress {

    /**
     * No additional information
     */
    public static final int INFO_NONE = 0;

    /**
     * This is an inbound address and cannot be used to initiate an outbound
     * connection to another peer
     */
    public static final int INFO_INBOUND = 1;

    /**
     * For which peer is this an address?
     */
    public PeerIdentity peer;

    /**
     * Name of the transport plugin enabling the communication using
     * this address.
     */
    public String transportName;

    /**
     * Binary representation of the address (plugin-specific).
     */
    public byte[] address;

    /**
     * Extended information about address.
     * Either INFO_NONE or INFO_INBOUND.
     */
    public int localInfo;
}
