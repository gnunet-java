/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.transport;

import org.gnunet.util.*;

public interface PeerAddressMonitorCallback {
    /**
     * Called with address information from transport.
     *
     * @param helloAddress the helloAddress
     * @param state the state of the peer
     * @param stateTimeout validity of the address state
     */
    void onPeerAddress(HelloAddress helloAddress, int state, AbsoluteTime stateTimeout);

    /**
     * Called when the given peer disconnected on transport
     * level.
     *
     * @param peerIdentity the peer that disconnected
     */
    void onPeerDisconnect(PeerIdentity peerIdentity);
}
