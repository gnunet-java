package org.gnunet.transport;

/**
 * ...
 *
 * @author Florian Dold
 */
public interface TryConnectCallback {
    void onDone();
}
