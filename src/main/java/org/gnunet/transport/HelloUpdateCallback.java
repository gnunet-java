package org.gnunet.transport;

import org.gnunet.hello.HelloMessage;

/**
 * ...
 *
 * @author Florian Dold
 */
public interface HelloUpdateCallback {
    void onHello(HelloMessage helloMessage);
}
