/*
 This file is part of GNUnet.
  Copyright (C) 2014 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.transport;


/**
 * Possible state of a neighbour.  Initially, we are #GNUNET_TRANSPORT_PS_NOT_CONNECTED.
 *
 * Then, there are two main paths. If we receive a CONNECT message, we give
 * the inbound address to ATS. After the check we ask ATS for a suggestion
 * (#GNUNET_TRANSPORT_PS_CONNECT_RECV_ATS). If ATS makes a suggestion, we
 * send our CONNECT_ACK and go to #GNUNET_TRANSPORT_PS_CONNECT_RECV_ACK.
 * If we receive a SESSION_ACK, we go to #GNUNET_TRANSPORT_PS_CONNECTED
 * (and notify everyone about the new connection). If the operation times out,
 * we go to #GNUNET_TRANSPORT_PS_DISCONNECT.
 *
 * The other case is where we transmit a CONNECT message first.  We
 * start with #GNUNET_TRANSPORT_PS_INIT_ATS.  If we get an address, we send
 * the CONNECT message and go to state #GNUNET_TRANSPORT_PS_CONNECT_SENT.
 * Once we receive a CONNECT_ACK, we go to #GNUNET_TRANSPORT_PS_CONNECTED
 * (and notify everyone about the new connection and send
 * back a SESSION_ACK).  If the operation times out, we go to
 * #GNUNET_TRANSPORT_PS_DISCONNECT.
 *
 * If the session is in trouble (i.e. transport-level disconnect or
 * timeout), we go to #GNUNET_TRANSPORT_PS_RECONNECT_ATS where we ask ATS for a new
 * address (we don't notify anyone about the disconnect yet).  Once we
 * have a new address, we enter #GNUNET_TRANSPORT_PS_RECONNECT_SENT and send a
 * CONNECT message.  If we receive a
 * CONNECT_ACK, we go to #GNUNET_TRANSPORT_PS_CONNECTED and nobody noticed that we had
 * trouble; we also send a SESSION_ACK at this time just in case.  If
 * the operation times out, we go to #GNUNET_TRANSPORT_PS_DISCONNECT (and notify everyone
 * about the lost connection).
 *
 * If ATS decides to switch addresses while we have a normal
 * connection, we go to #GNUNET_TRANSPORT_PS_CONNECTED_SWITCHING_CONNECT_SENT
 * and send a SESSION_CONNECT.  If we get a SESSION_ACK back, we switch the
 * primary connection to the suggested alternative from ATS, go back
 * to #GNUNET_TRANSPORT_PS_CONNECTED and send a SESSION_ACK to the other peer just to be
 * sure.  If the operation times out
 * we go to #GNUNET_TRANSPORT_PS_CONNECTED (and notify ATS that the given alternative
 * address is "invalid").
 *
 * Once a session is in #GNUNET_TRANSPORT_PS_DISCONNECT, it is cleaned up and then goes
 * to (#GNUNET_TRANSPORT_PS_DISCONNECT_FINISHED).  If we receive an explicit disconnect
 * request, we can go from any state to #GNUNET_TRANSPORT_PS_DISCONNECT, possibly after
 * generating disconnect notifications.
 *
 * Note that it is quite possible that while we are in any of these
 * states, we could receive a 'CONNECT' request from the other peer.
 * We then enter a 'weird' state where we pursue our own primary state
 * machine (as described above), but with the 'send_connect_ack' flag
 * set to 1.  If our state machine allows us to send a 'CONNECT_ACK'
 * (because we have an acceptable address), we send the 'CONNECT_ACK'
 * and set the 'send_connect_ack' to 2.  If we then receive a
 * 'SESSION_ACK', we go to #GNUNET_TRANSPORT_PS_CONNECTED (and reset 'send_connect_ack'
 * to 0).
 *
 */
class PeerState
{
  /**
   * Fresh peer or completely disconnected
   */
  public static final int NOT_CONNECTED = 0;

  /**
   * Asked to initiate connection, trying to get address from ATS
   */
  public static final int INIT_ATS = 1;

  /**
   * Sent CONNECT message to other peer, waiting for CONNECT_ACK
   */
  public static final int CONNECT_SENT = 2;

  /**
   * Received a CONNECT, asking ATS about address suggestions.
   */
  public static final int CONNECT_RECV_ATS = 3;

  /**
   * CONNECT request from other peer was CONNECT_ACK'ed, waiting for
   * SESSION_ACK.
   */
  public static final int CONNECT_RECV_ACK = 4;

  /**
   * Got our CONNECT_ACK/SESSION_ACK, connection is up.
   */
  public static final int CONNECTED = 5;

  /**
   * Connection got into trouble, rest of the system still believes
   * it to be up, but we're getting a new address from ATS.
   */
  public static final int RECONNECT_ATS = 6;

  /**
   * Sent CONNECT over new address (either by ATS telling us to switch
   * addresses or from RECONNECT_ATS); if this fails, we need to tell
   * the rest of the system about a disconnect.
   */
  public static final int RECONNECT_SENT = 7;

  /**
   * We have some primary connection, but ATS suggested we switch
   * to some alternative; we now sent a CONNECT message for the
   * alternative session to the other peer and waiting for a
   * CONNECT_ACK to make this our primary connection.
   */
  public static final int CONNECTED_SWITCHING_CONNECT_SENT = 8;

  /**
   * Disconnect in progress (we're sending the DISCONNECT message to the
   * other peer; after that is finished, the state will be cleaned up).
   */
  public static final int DISCONNECT = 9;

  /**
   * We're finished with the disconnect; and are cleaning up the state
   * now!  We put the struct into this state when we are really in the
   * task that calls 'free' on it and are about to remove the record
   * from the map.  We should never find a 'struct NeighbourMapEntry'
   * in this state in the map.  Accessing a 'struct NeighbourMapEntry'
   * in this state virtually always means using memory that has been
   * freed (the exception being the cleanup code in #free_neighbour()).
   */
  public static final int DISCONNECT_FINISHED = 10;
}
