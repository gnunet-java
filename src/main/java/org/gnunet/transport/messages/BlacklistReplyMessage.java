package org.gnunet.transport.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

@UnionCase(371)
public class BlacklistReplyMessage implements GnunetMessage.Body {
    @UInt32
    public boolean isAllowed;

    @NestedMessage
    public PeerIdentity peer;
}
