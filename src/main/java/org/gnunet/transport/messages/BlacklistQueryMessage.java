package org.gnunet.transport.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

@UnionCase(370)
public class BlacklistQueryMessage implements GnunetMessage.Body {
    @UInt32
    public byte reserved;

    @NestedMessage
    public PeerIdentity peer;
}
