package org.gnunet.transport.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

/**
 * ...
 *
 * @author Florian Dold
 */
@UnionCase(374)
public class RequestConnectMessage implements GnunetMessage.Body {
    @UInt32
    public int reserved;

    /**
     * Identity of the peer we would like to connect to.
     */
    @NestedMessage
    public PeerIdentity peer;
}
