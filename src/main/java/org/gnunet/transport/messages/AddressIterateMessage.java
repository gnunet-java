package org.gnunet.transport.messages;


import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;


/**
 * Message from the client to the transport service
 * asking for binary addresses known for a peer.
 */
@UnionCase(380)
public class AddressIterateMessage implements GnunetMessage.Body {
    /**
     * One shot call or continous replies?
     */
    @UInt32
    public boolean oneShot;

    /**
     * The identity of the peer to look up.
     */
    @NestedMessage
    public PeerIdentity peer;
}

