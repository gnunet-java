package org.gnunet.transport.messages;

import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

@UnionCase(369)
public class BlacklistInitMessage implements GnunetMessage.Body {
    // message body is empty
}
