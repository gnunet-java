package org.gnunet.peerinfo.messages;

import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * ...
 *
 * @author Florian Dold
 */
@UnionCase(333)
public class InfoEnd implements GnunetMessage.Body {

}
