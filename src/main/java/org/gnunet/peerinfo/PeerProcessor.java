package org.gnunet.peerinfo;

import org.gnunet.hello.HelloMessage;
import org.gnunet.util.PeerIdentity;

/**
 * Callback class to receive known peers and their HELLOs.
 */
public interface PeerProcessor {
    /**
     * Process a peer and its hello
     *
     * @param peerIdentity the peer
     * @param hello the hello of the peer
     */
    public void onPeer(PeerIdentity peerIdentity, HelloMessage hello);

    /**
     * Called to indicate the end of a peer list.
     */
    public void onEnd();
}
