/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.secretsharing;

import org.gnunet.secretsharing.messages.ClientDecryptMessage;
import org.gnunet.secretsharing.messages.DecryptDoneMessage;
import org.gnunet.util.AbsoluteTime;
import org.gnunet.util.Client;
import org.gnunet.util.Configuration;
import org.gnunet.util.RunaboutMessageReceiver;

/**
 * Cooperatively decrypt a ciphertext.
 */
public class Decryption {
    private Client client;
    private DecryptCallback decryptCallback;

    private class DecryptionReceiver extends RunaboutMessageReceiver {
        public void visit(DecryptDoneMessage m) {
            if (m.success != 0) {
                decryptCallback.onResult(m.plaintext);
            } else {
                decryptCallback.onResult(null);
            }
            client.disconnect();
            client = null;
        }
        @Override
        public void handleError() {
            decryptCallback.onResult(null);
        }
    }

    /**
     * Cooperatively decrypt a ciphertext encrypted with a
     * threshold key.
     *
     * @param configuration configuration to use for connecting to the
     *                      secretsharing service
     * @param share the local peer's share of the threshold key
     * @param ciphertext ciphertext to decrypt
     * @param start when should we start decrypting?
     * @param deadline when should decryption be finished?
     * @param decryptCallback called when the decryption finished, or
     *                        an error occurred.
     */
    public Decryption(Configuration configuration, Share share, Ciphertext ciphertext,
                      AbsoluteTime start, AbsoluteTime deadline, DecryptCallback decryptCallback) {
        this.decryptCallback = decryptCallback;
        client = new Client("secretsharing", configuration);
        client.installReceiver(new DecryptionReceiver());

        ClientDecryptMessage m = new ClientDecryptMessage();
        m.ciphertext = ciphertext;
        m.deadline = deadline.asMessage();
        m.start = start.asMessage();
        m.share = share;

        client.send(m);
    }

    public void disconnect() {
        if (null != client) {
            client.disconnect();
            client = null;
        }
    }
}
