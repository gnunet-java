package org.gnunet.secretsharing;

import org.gnunet.construct.*;
import org.gnunet.util.PeerIdentity;

/**
 * Share of the threshold secret key.  Contains both the
 * public key as well as one peer's private share.
 */
public class Share implements Message {
    /**
     * Threshold for the key this share belongs to.
     */
    @UInt16
    public int threshold;

    /**
     * Peers that have the share.
     */
    @UInt16
    public int numPeers;

    /**
     * Index of our peer in the list.
     */
    @UInt16
    public int myPeer;

    /**
     * Padding, always zero.
     */
    @UInt16
    public int padding;

    /**
     * Public key. Must correspond to the product of
     * the homomorphic share commitments.
     */
    @NestedMessage
    public ThresholdPublicKey publicKey;

    /**
     * Share of 'my_peer'
     */
    @NestedMessage
    public FieldElement myShare;

    /**
     * Peers that have a share of the secret.
     */
    @VariableSizeArray(lengthField = "numPeers")
    public PeerIdentity[] peerIdentities;

    /**
     * Sigma for each peer.  The sigma is pow(g,s)
     * where g is the generator and s is the peer's share.
     */
    @VariableSizeArray(lengthField = "numPeers")
    public FieldElement[] sigmas;

    @VariableSizeIntegerArray(lengthField = "numPeers", bitSize = 16, signed = false)
    public int[] originalIndices;
}
