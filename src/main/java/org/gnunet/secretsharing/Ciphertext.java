/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.secretsharing;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.Message;
import org.gnunet.util.BigIntegers;
import org.gnunet.util.Strings;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * ElGamal ciphertext.
 */
public class Ciphertext implements Message {
    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] c_1;

    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] c_2;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ciphertext that = (Ciphertext) o;

        if (!Arrays.equals(c_1, that.c_1)) return false;
        if (!Arrays.equals(c_2, that.c_2)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = c_1 != null ? Arrays.hashCode(c_1) : 0;
        result = 31 * result + (c_2 != null ? Arrays.hashCode(c_2) : 0);
        return result;
    }

    /**
     * Allocate the ciphertext with zeros.
     */
    public void allocate() {
        c_1 = new byte[Parameters.elgamalBits / 8];
        c_2 = new byte[Parameters.elgamalBits / 8];
    }

    @Override
    public String toString() {
        byte[] allBytes = new byte[c_1.length + c_2.length];
        System.arraycopy(c_1, 0, allBytes, 0, c_1.length);
        System.arraycopy(c_2, 0, allBytes, c_1.length, c_2.length);
        return Strings.dataToString(allBytes);
    }

    public static Ciphertext fromString(String s) {
        byte[] allBytes = new byte[2 * Parameters.elgamalBits / 8];
        if (!Strings.stringToData(s, allBytes))
            return null;
        Ciphertext ciphertext = new Ciphertext();
        ciphertext.allocate();
        System.arraycopy(allBytes, 0, ciphertext.c_1, 0, ciphertext.c_1.length);
        System.arraycopy(allBytes, ciphertext.c_1.length, ciphertext.c_2, 0, ciphertext.c_2.length);
        return ciphertext;
    }

    /**
     * Multiply two elgamal ciphertexts.
     *
     * @param v the other ciphertext
     * @return the product of two ciphertexts
     */
    public Ciphertext multiply(Ciphertext v) {
        BigInteger xc_1 = new BigInteger(1, this.c_1);
        BigInteger xc_2 = new BigInteger(1, this.c_2);
        BigInteger yc_1 = new BigInteger(1, v.c_1);
        BigInteger yc_2 = new BigInteger(1, v.c_2);
        Ciphertext ciphertext = new Ciphertext();
        ciphertext.c_1 = BigIntegers.serializeUnsigned(xc_1.multiply(yc_1).mod(Parameters.elgamalP),
                Parameters.elgamalBits);
        ciphertext.c_2 = BigIntegers.serializeUnsigned(xc_2.multiply(yc_2).mod(Parameters.elgamalP),
                Parameters.elgamalBits);
        return ciphertext;
    }

    /**
     * Get the ciphertext that is the identity for multiplication.
     *
     * @return multiplicative identity ciphertext
     */
    public static Ciphertext identity() {
        Ciphertext ciphertext = new Ciphertext();
        ciphertext.allocate();
        ciphertext.c_1 = BigIntegers.serializeUnsigned(BigInteger.ONE, Parameters.elgamalBits);
        ciphertext.c_2 = BigIntegers.serializeUnsigned(BigInteger.ONE, Parameters.elgamalBits);
        return ciphertext;
    }
}
