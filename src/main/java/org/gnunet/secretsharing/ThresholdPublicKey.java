/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.secretsharing;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.Message;
import org.gnunet.util.Strings;

import java.util.Arrays;

/**
 * Threshold public key.
 */
public class ThresholdPublicKey implements Message {

    @FixedSizeIntegerArray(signed = true, bitSize = 8, length = Parameters.elgamalBits / 8)
    public byte[] bits;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThresholdPublicKey that = (ThresholdPublicKey) o;
        return Arrays.equals(bits, that.bits);
    }

    @Override
    public int hashCode() {
        return bits != null ? Arrays.hashCode(bits) : 0;
    }

    public static ThresholdPublicKey fromString(String value) {
        ThresholdPublicKey pk = new ThresholdPublicKey();
        pk.bits = new byte[Parameters.elgamalBits / 8];
        Strings.stringToData(value, pk.bits);
        return pk;
    }

    @Override
    public String toString() {
        return Strings.dataToString(bits);
    }
}
