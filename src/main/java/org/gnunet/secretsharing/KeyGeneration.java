/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.secretsharing;

import org.gnunet.secretsharing.messages.GenerateMessage;
import org.gnunet.secretsharing.messages.SecretReadyMessage;
import org.gnunet.util.*;

/**
 * Key generation session.
 */
public class KeyGeneration {
    private Client client;
    private SecretReadyCallback secretReadyCallback;

    private class KeyGenerationReceiver extends RunaboutMessageReceiver {
        public void visit(SecretReadyMessage m) {
            secretReadyCallback.onSecretReady(m.share);
            client.disconnect();
            client = null;
        }
        @Override
        public void handleError() {
            secretReadyCallback.onSecretReady(null);
        }
    }

    /**
     * Generate a threshold key pair.
     *
     * @param configuration configuratio to use
     * @param peers peers in the session, the local peer is optional
     * @param sessionId session id
     * @param start when should we start to generate the key with other peers?
     * @param deadline when should the key generation be finished?
     * @param threshold minimum number of peers required to decrypt a ciphertext
     * @param secretReadyCallback called when the key has been generated or
     *                            an error occured
     */
    public KeyGeneration(Configuration configuration, PeerIdentity[] peers,
                         HashCode sessionId, AbsoluteTime start, AbsoluteTime deadline,
                         int threshold, SecretReadyCallback secretReadyCallback) {
        this.client = new Client("secretsharing", configuration);
        this.secretReadyCallback = secretReadyCallback;

        client.installReceiver(new KeyGenerationReceiver());

        GenerateMessage m = new GenerateMessage();
        m.start = start.asMessage();
        m.deadline = deadline.asMessage();
        m.threshold = threshold;
        m.numPeers = peers.length;
        m.peers = peers;
        m.sessionId = sessionId;

        client.send(m);
    }

    /**
     * Disconnect from the secretsharing service and onCancel the operation.
     * The SecretReadyCallback will not be called after calling disconnect.
     */
    public void disconnect() {
        if (null != client) {
            client.disconnect();
            client = null;
        }
    }
}
