/*

   This file is part of GNUnet.
   Copyright (C) 2014 Christian Grothoff (and other contributing authors)

   GNUnet is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation; either version 3, or (at your
   option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNUnet; see the file COPYING.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

 */

package org.gnunet.secretsharing.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt16;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.VariableSizeArray;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.HashCode;
import org.gnunet.util.PeerIdentity;


@UnionCase(780)
public class GenerateMessage implements GnunetMessage.Body {
    @NestedMessage
    public HashCode sessionId;

    @NestedMessage
    public AbsoluteTimeMessage start;

    @NestedMessage
    public AbsoluteTimeMessage deadline;

    @UInt16
    public int threshold;

    @UInt16
    public int numPeers;

    @VariableSizeArray(lengthField = "numPeers")
    public PeerIdentity[] peers;
}
