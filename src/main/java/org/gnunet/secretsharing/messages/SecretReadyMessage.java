/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.secretsharing.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UnionCase;
import org.gnunet.secretsharing.Share;
import org.gnunet.util.GnunetMessage;

/**
 * Sent by the service to the client when the key generation
 * has been successful.
 */
@UnionCase(783)
public class SecretReadyMessage implements GnunetMessage.Body {
    /**
     * Share for the local peer.
     */
    @NestedMessage
    public Share share;
}
