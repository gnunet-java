/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.core.messages;

import org.gnunet.construct.IntegerFill;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;


@UnionCase(64)
public class InitMessage implements GnunetMessage.Body {
    /*
    * Options used to tell core what kind of traffic notify messages we are interested in.
    */
    private final static int
            OPTION_FULL_INBOUND = 8,
            OPTION_HDR_INBOUND = 16,
            OPTION_FULL_OUTBOUND = 32,
            OPTION_HDR_OUTBOUND = 64;

    @UInt32
    public long options;

    @IntegerFill(signed = false, bitSize = 16)
    public int[] interested;
}
