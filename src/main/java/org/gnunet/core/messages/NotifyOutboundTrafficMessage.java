/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.core.messages;

import org.gnunet.construct.*;
import org.gnunet.util.ATSInformation;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;


@UnionCase(71)
public class NotifyOutboundTrafficMessage implements GnunetMessage.Body {
    /**
     * Number of ATS key-value pairs that follow this struct
     * (excluding the 0-terminator).
     */
    @UInt32
    public long atsCount;

    /**
     * Identity of the receiver or sender.
     */
    @NestedMessage
    public PeerIdentity peer;

    @VariableSizeArray(lengthField = "atsCount")
    public ATSInformation[] atsRest;

    @NestedMessage(newFrame = true)
    public GnunetMessage.Header payloadHeader;

    /**
     * The (optional) message body corresponding to payloadHeader.
     * Not typed as GnunetMessage.Body because the message type may not be known by this
     * peer.
     */
    @FillWith @UInt8
    public byte[] payloadBody;

}
