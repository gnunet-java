/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.core.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt16;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

/**
 * Core notifying client that it is allowed to now
 * transmit a message to the given target
 * (response to GNUNET_MESSAGE_TYPE_CORE_SEND_REQUEST).
 */
@UnionCase(75)
public class SendMessageReady implements GnunetMessage.Body {
    /**
     * How many bytes are allowed for transmission?
     * Guaranteed to be at least as big as the requested size,
     * or ZERO if the getRequestIdentifier is rejected (will timeout,
     * peer disconnected, queue full, etc.).
     */
    @UInt16
    public int size;

    /**
     * smrId from the getRequestIdentifier.
     */
    @UInt16
    public int smrId;

    /**
     * Identity of the intended target.
     */
    @NestedMessage
    public PeerIdentity peer;
}
