/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.core.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

/**
 * Client asking core to transmit a particular message to a particular
 * target (response to GNUNET_MESSAGE_TYPE_CORE_SEND_READY).
 */
@UnionCase(76)
public class SendMessage implements GnunetMessage.Body {
    /**
     * How important is this message?
     */
    @UInt32
    public long priority;

    /**
     * By what time would the sender really like to see this
     * message transmitted?
     */
    @NestedMessage
    public AbsoluteTimeMessage deadline;

    /**
     * Identity of the intended receiver.
     */
    @NestedMessage
    public PeerIdentity peer;

    /**
     * GNUNET_YES if corking is allowed, GNUNET_NO if not.
     */
    @UInt32
    public int cork;

    /**
     * Always 0.
     */
    @UInt32
    public int reserved;

    @NestedMessage(newFrame = true)
    public GnunetMessage payloadMessage;

}
