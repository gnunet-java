package org.gnunet.core;

import org.gnunet.util.PeerIdentity;

/**
 * Continuation that receives the peer's identity.
 */
public interface PeerIdentityContinuation {
    /**
     * Called with the peer identity.
     *
     * @param peerIdentity the peer identity
     */
    public void cont(PeerIdentity peerIdentity);
}
