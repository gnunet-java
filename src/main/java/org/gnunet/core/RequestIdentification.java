package org.gnunet.core;

import org.gnunet.util.PeerIdentity;


final class RequestIdentification {
    public final int requestIdentifier;
    public final PeerIdentity peerIdentity;

    public RequestIdentification(int requestIdentifier, PeerIdentity peerIdentity) {
        this.requestIdentifier = requestIdentifier;
        this.peerIdentity = peerIdentity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestIdentification that = (RequestIdentification) o;

        if (requestIdentifier != that.requestIdentifier) return false;
        if (!peerIdentity.equals(that.peerIdentity)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = requestIdentifier;
        result = 31 * result + peerIdentity.hashCode();
        return result;
    }
}
