/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.hello;

import org.gnunet.construct.*;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.crypto.EddsaPublicKey;

/**
 * A HELLO message is used to exchange information about
 * transports with other peers.  This struct is always
 * followed by the actual network addresses which have
 * the format:
 *
 * 1) transport-name (0-terminated)
 * 2) address-length (uint16_t, network byte order; possibly
 *    unaligned!)
 * 3) address expiration (GNUNET_TIME_AbsoluteNBO); possibly
 *    unaligned!)
 * 4) address (address-length bytes; possibly unaligned!)
 *
 * @author Florian Dold
 */
@UnionCase(17)
public class HelloMessage implements GnunetMessage.Body {
    @UInt32
    public int friendOnly;

    @NestedMessage
    public EddsaPublicKey publicKey;

    @FillWith @UInt8
    public byte[] addresses;
}
