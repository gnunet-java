/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.peerstore;

import org.gnunet.peerstore.messages.IterateEndMessage;
import org.gnunet.peerstore.messages.IterateRecordMessage;
import org.gnunet.peerstore.messages.StoreMessage;
import org.gnunet.peerstore.messages.WatchRecordMessage;
import org.gnunet.requests.MatchingRequestContainer;
import org.gnunet.requests.RequestIdentifier;
import org.gnunet.requests.SequentialRequestContainer;
import org.gnunet.requests.TimeoutHandler;
import org.gnunet.util.*;

/**
 * API for the GNUnet peerstore service.
 */
public class Peerstore {

	/**
	 * Client connecting us to the statistics service.
	 */
	private Client client;

	/**
	 * All request to the service for iterating records.
	 */
	private final SequentialRequestContainer<IterateRequest> iterateRequests;

	/**
	 * All requests to the service for watching a value.
	 */
	private final MatchingRequestContainer<HashCode, WatchRequest> watchRequests;

	/**
	 * Are we currently destroying the client?
	 */
	private boolean destroyRequested;

	/**
	 * Messages from the statistics service are dispatched to an instance of
	 * this class.
	 */
	public class PeerstoreMessageReceiver extends RunaboutMessageReceiver {

		public void visit(IterateRecordMessage m) {
			RequestIdentifier<IterateRequest> req = iterateRequests
					.getRequestIdentifier();
			if (null != req)
				req.getRequest().receiver.onReceive(m.sub_system, m.peer,
						m.key, m.value, new AbsoluteTime(m.expiry.value));
		}

		public void visit(IterateEndMessage m) {
			RequestIdentifier<IterateRequest> req = iterateRequests
					.getRequestIdentifier();
			if (null != req) {
				req.retire();
				req.getRequest().receiver.onDone();
			}
		}

		public void visit(WatchRecordMessage m) {
			RequestIdentifier<WatchRequest> ri = watchRequests
					.getRequestIdentifier(WatchRequest.getHash(m.sub_system,
							m.peer, m.key));
			WatchRequest r = ri.getRequest();
			if (null != r) {
				r.watcher.onReceive(m.sub_system, m.peer, m.key, m.value,
						new AbsoluteTime(m.expiry.value));
			}
		}

		@Override
		public void handleError() {
			if (null == client)
				throw new AssertionError();
			if (!destroyRequested) {
				client.reconnect();
				iterateRequests.restart();
				watchRequests.restart();
			}
		}

	}

	/**
	 * Create a connection to the peerstore service.
	 * 
	 * @param cfg
	 *            configuration to use
	 */
	public Peerstore(Configuration cfg) {
		client = new Client("peerstore", cfg);
		client.installReceiver(new PeerstoreMessageReceiver());
		iterateRequests = new SequentialRequestContainer<IterateRequest>(client);
		watchRequests = new MatchingRequestContainer<HashCode, WatchRequest>(
				client);
	}

	/**
	 * Store a new entry in the PEERSTORE. Note that stored entries can be lost
	 * in some cases such as power failure.
	 * 
	 * @param sub_system
	 *            name of the sub system
	 * @param peer
	 *            Peer Identity
	 * @param key
	 *            entry key
	 * @param value
	 *            entry value BLOB
	 * @param expiry
	 *            absolute time after which the entry is (possibly) deleted
	 * @param options
	 *            options specific to the storage operation
	 */
	public void store(final String sub_system, final PeerIdentity peer,
			final String key, final byte[] value, AbsoluteTime expiry,
			StoreOption options) {
		if (destroyRequested || client == null)
			throw new AssertionError("already destroyed");
		StoreMessage sm = new StoreMessage();
		sm.peer_set = 1;
		sm.peer = peer;
		sm.sub_system_size = sub_system.length() + 1;
		sm.key_size = key.length() + 1;
		sm.value_size = value.length;
		sm.expiry = new AbsoluteTimeMessage(expiry);
		sm.options = options.val;
		sm.sub_system = sub_system;
		sm.key = key;
		sm.value = value;
		client.send(sm);
	}

	/**
	 * Iterate over records matching supplied key information
	 * 
	 * @param sub_system
	 *            name of sub system
	 * @param peer
	 *            Peer identity (can be NULL)
	 * @param key
	 *            entry key string (can be NULL)
	 * @param timeout
	 *            time after which the iterate request is canceled
	 * @param receiver
	 *            Object that will receive request results
	 * @return Handle to iteration request
	 */
	public Cancelable iterate(final String sub_system, final PeerIdentity peer,
			final String key, final RelativeTime timeout,
			final PeerstoreReceiver receiver) {
		if (destroyRequested || client == null)
			throw new AssertionError("already destroyed");
		RequestIdentifier<IterateRequest> identifier = iterateRequests
				.addRequest(new IterateRequest(sub_system, peer, key, receiver));
		identifier.setTimeout(timeout, new TimeoutHandler() {

			@Override
			public void onTimeout() {
				receiver.onTimeout();

			}
		});
		return identifier;
	}

	/**
	 * Request watching a given key User will be notified with any new values
	 * added to key
	 * 
	 * @param sub_system
	 *            name of sub system
	 * @param peer
	 *            Peer identity
	 * @param key
	 *            entry key string
	 * @param watcher
	 *            Receiver for watch records
	 * @return Handle to watch request
	 */
	public Cancelable watch(final String sub_system, final PeerIdentity peer,
			final String key, PeerstoreWatcher watcher) {
		if (destroyRequested || null == client)
			throw new AssertionError("already destroyed");
		WatchRequest r = new WatchRequest(sub_system, peer, key, watcher);
		return watchRequests.addRequest(r.hash, r);
	}

	/**
	 * Destroy handle to the peerstore service.
	 */
	public void destroy() { // TODO: imeplement syncfirst
		if (destroyRequested)
			throw new AssertionError("already destroyed");
		destroyRequested = true;
		client.disconnect();
		client = null;
	}
}
