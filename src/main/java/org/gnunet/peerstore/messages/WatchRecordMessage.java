package org.gnunet.peerstore.messages;

import org.gnunet.construct.FillWith;
import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt16;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt8;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

@UnionCase(825)
public class WatchRecordMessage implements GnunetMessage.Body {
	@UInt16
	public int peer_set;
	@NestedMessage
	public PeerIdentity peer;
	@UInt16
	public int sub_system_size;
	@UInt16
	public int key_size;
	@UInt16
	public int value_size;
	@NestedMessage
	public AbsoluteTimeMessage expiry;
	@UInt32
	public int options;
	@ZeroTerminatedString
	public String sub_system;
	@ZeroTerminatedString
	public String key;
	@FillWith
	@UInt8
	public byte[] value;
}
