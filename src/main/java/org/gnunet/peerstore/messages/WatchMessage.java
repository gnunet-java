package org.gnunet.peerstore.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.HashCode;

@UnionCase(824)
public class WatchMessage implements GnunetMessage.Body {
	@NestedMessage
	public HashCode keyhash;
}
