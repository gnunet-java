/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.peerstore;

import org.gnunet.mq.Envelope;
import org.gnunet.peerstore.messages.IterateMessage;
import org.gnunet.requests.Request;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.PeerIdentity;

class IterateRequest extends Request {
	private final String sub_system;
	private final PeerIdentity peer;
	private final String key;
	public final PeerstoreReceiver receiver;

	public IterateRequest(String sub_system, PeerIdentity peer, String key,
			PeerstoreReceiver receiver) {
		this.sub_system = sub_system;
		this.peer = peer;
		this.key = key;
		this.receiver = receiver;
	}

	@Override
	public Envelope assembleRequest() {
		IterateMessage im = new IterateMessage();
		im.sub_system_size = this.sub_system.length() + 1;
		im.sub_system = this.sub_system;
		if (null == this.peer) {
			im.peer_set = 0;
			im.peer = new PeerIdentity();
		} else {
			im.peer_set = 1;
			im.peer = peer;
		}
		if (null == this.key) {
			im.key = null;
			im.key_size = 0;
		} else {
			im.key_size = this.key.length() + 1;
			im.key = this.key;
		}
		im.value_size = 0;
		im.value = new byte[0];
		im.expiry = new AbsoluteTimeMessage();
		im.options = 0;
		return new Envelope(im);
	}

	public void cancel() {
	}

}
