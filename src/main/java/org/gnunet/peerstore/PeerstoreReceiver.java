/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.peerstore;

import org.gnunet.util.AbsoluteTime;
import org.gnunet.util.PeerIdentity;

public interface PeerstoreReceiver {
	/**
	 * Called when having received a peerstore record from the service.
	 */
	public void onReceive(String sub_system, PeerIdentity peer, String key,
			byte[] value, AbsoluteTime expiry);

	/**
	 * Called when peerstore request times out. Never called for watchers.
	 */
	public void onTimeout();

	/**
	 * Called when all values for the request have been received. Never called
	 * for watchers.
	 */
	public void onDone();
}
