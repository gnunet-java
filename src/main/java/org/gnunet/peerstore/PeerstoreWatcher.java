package org.gnunet.peerstore;

import org.gnunet.util.AbsoluteTime;
import org.gnunet.util.PeerIdentity;

public interface PeerstoreWatcher {

	public void onReceive(String sub_system, PeerIdentity peer, String key,
			byte[] value, AbsoluteTime expiry);
}
