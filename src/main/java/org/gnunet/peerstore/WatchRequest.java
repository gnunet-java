/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.peerstore;

import java.nio.ByteBuffer;

import org.gnunet.mq.Envelope;
import org.gnunet.peerstore.messages.WatchMessage;
import org.gnunet.requests.Request;
import org.gnunet.util.HashCode;
import org.gnunet.util.PeerIdentity;

class WatchRequest extends Request {
	public final String sub_system;
	public final PeerIdentity peer;
	public final String key;
	public final PeerstoreWatcher watcher;
	public final HashCode hash;

	public static HashCode getHash(String sub_system, PeerIdentity peer,
			String key) {
		int block_size = 0;
		block_size += sub_system.length() + 1;
		block_size += peer.data.length;
		block_size += key.length() + 1;

		byte[] block = new byte[block_size];
		ByteBuffer target = ByteBuffer.wrap(block);
		target.put(sub_system.getBytes());
		target.put((byte)'\0');
		target.put(peer.data);
		target.put(key.getBytes());
		target.put((byte)'\0');

		return HashCode.hash(block);
	}

	public WatchRequest(String sub_system, PeerIdentity peer, String key,
			PeerstoreWatcher watcher) {
		this.sub_system = sub_system;
		this.peer = peer;
		this.key = key;
		this.watcher = watcher;
		this.hash = WatchRequest.getHash(sub_system, peer, key);
	}

	@Override
	public Envelope assembleRequest() {
		WatchMessage wm = new WatchMessage();
		wm.keyhash = this.hash;
		return new Envelope(wm);
	}

	@Override
	public void cancel() {
		// TODO: send a watch cancel request
	}

}
