package org.gnunet.nse;

import org.gnunet.construct.DoubleValue;
import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.AbsoluteTimeMessage;
import org.gnunet.util.GnunetMessage;

/**
* ...
*
* @author Florian Dold
*/
@UnionCase(323)
public class UpdateMessage implements GnunetMessage.Body {
    @UInt32
    public int reserved;

    @NestedMessage
    public AbsoluteTimeMessage timestamp;

    @DoubleValue
    public double sizeEstimate;

    @DoubleValue
    public double stdDeviation;
}
