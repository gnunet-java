package org.gnunet.nse;

import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
* ...
*
* @author Florian Dold
*/
@UnionCase(321)
public class StartMessage implements GnunetMessage.Body {}
