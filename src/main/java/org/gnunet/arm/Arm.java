/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.arm;

import org.gnunet.util.Client;
import org.gnunet.util.Configuration;

/**
 * API for the Automatic Restart Manager (ARM)
 */
public class Arm {
    private final Configuration configuration;
    private Client client;
    public Arm(Configuration configuration) {
        this.configuration = configuration;
        client = new Client("arm", configuration);
    }

    /**
     * Disconnect from ARM, the Arm object may not be used afterwards.
     */
    public void disconnect() {
        client.disconnect();
    }
    public void requestServiceList(ServiceListHandler serviceListHandler) {

    }

    /**
     * Request a service to be stopped.
     * Stopping arm itself will not invalidate its handle, and
     * ARM API will try to restore connection to the ARM service,
     * even if ARM connection was lost because you asked for ARM to be stopped.
     * Call Arm.disconnect to free the handle and prevent
     * further connection attempts.
     *
     * @param serviceName name of the service
     * @param resultHandler called with the result of the getRequestIdentifier
     */
    public void requestServiceStop(String serviceName, ResultHandler resultHandler) {

    }

    /**
     * Request for a service to be started.
     *
     * @param serviceName name of the service
     */
    public void requestServiceStart(String serviceName, ResultHandler resultHandler) {

    }
}
