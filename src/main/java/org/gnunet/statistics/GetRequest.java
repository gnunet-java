package org.gnunet.statistics;

import org.gnunet.mq.Envelope;
import org.gnunet.requests.Request;
import org.gnunet.statistics.messages.GetMessage;

class GetRequest extends Request {
    private final String subsystem;
    private final String name;
    public final StatisticsReceiver receiver;

    public GetRequest(String subsystem, String name, StatisticsReceiver receiver) {
        this.subsystem = subsystem;
        this.name = name;
        this.receiver = receiver;
    }

    @Override
    public Envelope assembleRequest() {
        GetMessage m = new GetMessage();
        m.subsystemName = subsystem;
        m.statisticsName = name;
        return new Envelope(m);
    }

    public void cancel() {
        // nothing we can do here (statistics service has no message to cancel requests)
    }
}
