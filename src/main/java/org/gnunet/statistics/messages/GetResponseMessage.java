/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.statistics.messages;

import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;

/**
 * service --> client
 *
 *
 */
@UnionCase(170)
public class GetResponseMessage implements GnunetMessage.Body {
    /**
     * Unique numerical identifier for the value (will
     * not change during the same client-session).  Highest
     * bit will be set for persistent values.
     */
    @UInt32
    public long uid;
    @UInt64
    public long value;
    @ZeroTerminatedString
    public String subsystemName;
    @ZeroTerminatedString
    public String statisticName;
}
