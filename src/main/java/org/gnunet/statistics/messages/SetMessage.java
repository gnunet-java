/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.statistics.messages;

import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;


/**
 * Sent to the service by the client to set a statistics value.
 */
@UnionCase(168)
public class SetMessage implements GnunetMessage.Body {
    public final static int SETFLAG_RELATIVE = 1;
    public final static int SETFLAG_PERSIST = 2;
    @UInt32
    public int flags;
    @UInt64
    public long value;
    @ZeroTerminatedString
    public String subsystemName;
    @ZeroTerminatedString
    public String statisticName;
}
