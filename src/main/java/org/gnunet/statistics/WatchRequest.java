package org.gnunet.statistics;

import org.gnunet.mq.Envelope;
import org.gnunet.requests.Request;
import org.gnunet.statistics.messages.WatchMessage;


class WatchRequest extends Request {
    public final String subsystem;
    public final String name;
    public final StatisticsWatcher watcher;

    public WatchRequest(String subsystem, String name, StatisticsWatcher watcher) {
        this.subsystem = subsystem;
        this.name = name;
        this.watcher = watcher;
    }

    @Override
    public Envelope assembleRequest() {
        WatchMessage m = new WatchMessage();
        m.statisticsName = name;
        m.subsystemName = subsystem;
        return new Envelope(m);
    }

    @Override
    public void cancel() {
        // do nothing, incoming changes with the watch id of this request
        // will simply be ignured
    }
}
