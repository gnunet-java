package org.gnunet.statistics;

/**
 * Listener for statistics changes.
 */
public interface StatisticsWatcher {
    /**
     * Called when receiving a change notification for a statistics value.
     *
     * @param subsystem subsystem of the value that changed
     * @param name name of the value that changed
     * @param value new value
     */
    public void onReceive(String subsystem, String name, long value);
}
