/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.statistics;


/**
 * Handler for statistics results.
 */
public interface StatisticsReceiver {
    /**
     * Called when having received a statistics value from the service.
     *
     * @param subsystem subsystem of the value
     * @param name name of the value
     * @param value the value
     */
    public void onReceive(String subsystem, String name, long value);

    /**
     * Called when a statistics request times out.  Never called
     * for watchers.
     */
    public void onTimeout();

    /**
     * Called when all values for the request have been received.
     * Never called for watchers.
     */
    public void onDone();
}
