package org.gnunet.gns.records;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.crypto.EcdsaPublicKey;

/**
 * A GNS PKEY record.
 */
@UnionCase(65536)
public class PkeyRecordData implements RecordData {

    @NestedMessage
    public EcdsaPublicKey publicKey;

    @SuppressWarnings("UnusedDeclaration")
    public static String recordTypeString = "PKEY";

    public static PkeyRecordData fromString(String s) {
        EcdsaPublicKey publicKey = EcdsaPublicKey.fromString(s);
        if (null == publicKey) {
            return null;
        }
        PkeyRecordData recordData = new PkeyRecordData();
        recordData.publicKey = publicKey;
        return recordData;
    }

    @Override
    public String asRecordString() {
        return publicKey.toString();
    }
}
