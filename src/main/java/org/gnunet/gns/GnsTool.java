package org.gnunet.gns;


import org.gnunet.identity.Identity;
import org.gnunet.identity.IdentityCallback;
import org.gnunet.util.Program;
import org.gnunet.util.getopt.Argument;
import org.gnunet.util.getopt.ArgumentAction;

/**
 * Command line tool for GNS.
 */
public class GnsTool {
    public static void main(String[] args) {
        int ret = new Program() {
            @Argument(
                    shortname = "u",
                    longname = "lookup",
                    action = ArgumentAction.STORE_STRING,
                    description = "Lookup a record for the given name")
            String name;

            @Argument(
                    shortname = "z",
                    longname = "zone",
                    action = ArgumentAction.STORE_STRING,
                    description = "Lookup a record in the given zone")
            String zone = "master-zone";

            @Argument(
                    shortname = "t",
                    longname = "type",
                    action = ArgumentAction.STORE_STRING,
                    description = "Lookup a record of the given type (defaut: A)")
            String type = "A";

            @Override
            protected void run() {
                if (null == name) {
                    System.err.println("no name given");
                    setReturnValue(1);
                    return;
                }

                final long typeId = GnsRecord.getIdFromString(type);
                if (typeId < 0) {
                    System.err.println(String.format("type '%s' not known", type));
                    setReturnValue(1);
                    return;
                }

                System.out.println("looking for records of type id " + typeId);

                Identity.lookup(getConfiguration(), zone, new IdentityCallback() {
                    @Override
                    public void onEgo(Identity.Ego ego) {
                        System.out.println("looking in zone " + ego.getPublicKey());
                        final Gns gns = new Gns(getConfiguration());
                        gns.lookup(name, ego.getPublicKey(), typeId, 0, null, new LookupResultProcessor() {
                            @Override
                            public void process(GnsRecord[] records) {
                                System.out.println("got " + records.length + " records");
                                gns.disconnect();
                                for (GnsRecord record : records) {
                                    String s = record.getRecordData().asRecordString();
                                    System.out.println(
                                            String.format("Type: %s, Value: %s", record.recordType, s));
                                }
                            }
                        });
                    }

                    @Override
                    public void onError(String errorMessage) {
                        System.err.println("could not look up zone ego '" + name + "': " + errorMessage);
                    }
                });

            }
        }.start(args);
        System.exit(ret);
    }
}
