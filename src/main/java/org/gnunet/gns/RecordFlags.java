/*
 This file is part of GNUnet.
  Copyright (C) 2014 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.gns;

/**
 * Flags for GNS records.
 */
public interface RecordFlags {
    /**
     * No special options.
     */
    public static final int NONE = 0;
    /**
     * No special options.
     */
    public static final int PRIVATE = 2;
    /**
     * This record was added automatically by the system
     * and is pending user confimation.
     */
    public static final int PENDING = 4;
    /**
     * This expiration time of the record is a relative
     * time (not an absolute time).
     */
    public static final int EXPIRATION = 8;
    /**
     * This record should not be used unless all (other) records with an absolute
     * expiration time have expired.
     */
    public static final int SHADOW_RECORD = 16;
}
