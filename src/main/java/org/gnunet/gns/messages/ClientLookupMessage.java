/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.gns.messages;

import org.gnunet.construct.*;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.crypto.EcdsaPrivateKey;
import org.gnunet.util.crypto.EcdsaPublicKey;

/**
 * Message from client to GNS service to lookup records.
 */
@UnionCase(500)
public class ClientLookupMessage implements GnunetMessage.Body {
    /**
     * Unique identifier for this getRequestIdentifier (for key collisions).
     */
    @UInt32
    public long id;

    /**
     * Zone that is to be used for lookup
     */
    @NestedMessage
    public EcdsaPublicKey zone;
    /**
     * Only check cached results
     */
    @Int16
    public int onlyCached;

    /**
     * Is a shorten key attached?
     */
    @Int16
    public int haveKey;

    /**
     * the type of record to look up
     */
    @Int32
    public long type;

    /**
     * The key for shorten, if haveKey is set
     */
    @NestedMessage
    public EcdsaPrivateKey shortenKey;

    /**
     * The name.
     */
    @ZeroTerminatedString
    public String name;
}
