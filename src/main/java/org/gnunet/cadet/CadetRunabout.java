package org.gnunet.cadet;

import org.grothoff.Runabout;

/**
 * ...
 *
 * @author Florian Dold
 */
public class CadetRunabout extends Runabout {
    private Cadet.Channel sender;
    /* package private */ void setSender(Cadet.Channel sender) {
        this.sender = sender;
    }
    public Cadet.Channel getSender() {
        return sender;
    }
}

