package org.gnunet.cadet;

import org.gnunet.util.PeerIdentity;

/**
 * ...
 *
 * @author Florian Dold
 */
public interface ConnectHandler {
    public void onConnect(Cadet.Channel channel, PeerIdentity peer);
}
