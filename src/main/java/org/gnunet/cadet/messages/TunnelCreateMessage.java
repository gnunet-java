package org.gnunet.cadet.messages;

import org.gnunet.construct.NestedMessage;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

/**
 * FIXME
 *
 * @author Florian Dold
 */
@UnionCase(273)
public class TunnelCreateMessage implements GnunetMessage.Body {
    @UInt32
    public long tunnelId;

    @NestedMessage(optional = false)
    public PeerIdentity otherEnd;

    @UInt32
    public int port;

    @UInt32
    public int opt;
}
