package org.gnunet.cadet.messages;

import org.gnunet.construct.IntegerFill;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * Allows a client to register with the service.
 *
 * Direction: client -> service
 *
 * @author Florian Dold
 */
@UnionCase(272)
public class ClientConnectMessage implements GnunetMessage.Body {
    @IntegerFill(signed = false, bitSize = 32)
    public int[] applicationList;
}
