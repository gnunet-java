package org.gnunet.cadet.messages;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * ...
 *
 * @author Florian Dold
 */
@UnionCase(274)
public class TunnelDestroyMessage implements GnunetMessage.Body {
    @UInt32
    public long tunnelId;

    @FixedSizeIntegerArray(bitSize = 8, signed = false, length = 32)
    public byte[] reserved;

    @UInt32
    public int port;

    @UInt32
    public int opt;
}
