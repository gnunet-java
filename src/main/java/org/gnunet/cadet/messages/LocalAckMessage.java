package org.gnunet.cadet.messages;

import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * ...
 *
 * @author Florian Dold
 */
@UnionCase(286)
public class LocalAckMessage implements GnunetMessage.Body {
    @UInt32
    public long tid;
}
