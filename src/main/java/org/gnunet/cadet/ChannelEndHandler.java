package org.gnunet.cadet;

/**
 * ...
 *
 * @author Florian Dold
 */
public interface ChannelEndHandler {
    /**
     * Called once a channel has been destroyed.
     * The given channel can not be used anymore, and is only provided
     * to identify the channel that has been destroyed.
     *
     * @param channel channel that has been destroyed
     */
    void onChannelEnd(Cadet.Channel channel);
}
