package org.gnunet.cadet;

import org.gnunet.util.PeerIdentity;

/**
 * ...
 *
 * @author Florian Dold
 */
public interface InboundChannelHandler {
    void onInboundChannel(Cadet.Channel channel, PeerIdentity initiator);
}
