/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.cadet;

import org.gnunet.construct.FixedSizeIntegerArray;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * Message sent by the server to indicate that a tunnel could not
 * be created.
 *
 * (GNUNET_MESSAGE_TYPE_CADET_CHANNEL_NACK)
 *
 * @author Florian Dold
 */
@UnionCase(276)
public class RejectMessage implements GnunetMessage.Body {
    @UInt32
    public long tunnelId;

    @FixedSizeIntegerArray(bitSize = 8, signed = false, length = 32)
    public byte[] reserved;
    
    @UInt32
    public int port;

    @UInt32
    public int opt;
}
