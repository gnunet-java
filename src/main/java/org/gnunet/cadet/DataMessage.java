package org.gnunet.cadet;

import org.gnunet.construct.FillWith;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt8;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * ...
 *
 * @author Florian Dold
 */
@UnionCase(285)
public class DataMessage implements GnunetMessage.Body {
    @UInt32
    public long tid;
    @FillWith
    @UInt8
    public byte[] payload;
}
