/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */


package org.gnunet.requests;

import com.google.common.collect.Maps;
import org.gnunet.mq.MessageQueue;
import org.gnunet.util.Cancelable;

import java.util.Map;


/**
 * Container for requests that are responded to with a matching getRequestIdentifier identification
 */
public class MatchingRequestContainer<K, T extends Request> extends RequestContainer {
    /**
     * All queued requests.
     */
    private Map<K,Identifier> requests = Maps.newHashMap();

    /**
     * Message queue to send to requests over.
     */
    private final MessageQueue mq;

    public MatchingRequestContainer(MessageQueue mq) {
        this.mq = mq;
    }

    private class Identifier extends SimpleRequestIdentifier<T> {
        final K key;

        public Identifier(T request, K key) {
            super(request);
            this.key = key;
        }

        @Override
        public void retire() {
            super.retire();
            requests.remove(key);
        }
    }

    public Cancelable addRequest(K key, final T request) {
        if (requests.containsKey(key))
            throw new AssertionError("key already present in getRequestIdentifier container");
        Identifier identifier = new Identifier(request, key);
        requests.put(key, identifier);
        identifier.send(mq);
        return identifier;
    }

    @Override
    public void restart() {
        Map<K, Identifier> requestsOld = requests;
        requests = Maps.newHashMap();
        for (Map.Entry<K,Identifier> e : requestsOld.entrySet()) {
            addRequest(e.getKey(), e.getValue().getRequest());
        }
    }

    public RequestIdentifier<T> getRequestIdentifier(K key) {
        return requests.get(key);
    }

    public T getAndRetireRequest(K key) {
        RequestIdentifier<T> i = getRequestIdentifier(key);
        if (null == i)
            return null;
        i.retire();
        return i.getRequest();
    }
}
