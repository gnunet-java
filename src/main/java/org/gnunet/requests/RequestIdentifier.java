/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.requests;

import org.gnunet.util.Cancelable;
import org.gnunet.util.RelativeTime;

/**
 * Identifies a queued getRequestIdentifier.
 */
public interface RequestIdentifier<T extends Request> extends Cancelable {

    /**
     * Cease being an active getRequestIdentifier.  Effectively removes the
     * getRequestIdentifier from its queue.
     * In contrast to 'cancel', this does not invoke the the cancel
     * action of the underlying request.
     */
    void retire();

    void setTimeout(RelativeTime timeout, TimeoutHandler timeoutHandler);

    T getRequest();

    /**
     * Actively cancel the request.
     */
    void cancel();
}
