/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.requests;

import org.gnunet.mq.Envelope;
import org.gnunet.mq.MessageQueue;
import org.gnunet.mq.NotifySentHandler;
import org.gnunet.util.Cancelable;
import org.gnunet.util.RelativeTime;
import org.gnunet.util.Scheduler;

abstract class SimpleRequestIdentifier<T extends Request> implements RequestIdentifier<T> {
    private final T request;
    /**
     * Has the message been queued for sending?
     */
    boolean queued;
    /**
     * Cancel sending the message via the message queue.
     */
    private Cancelable sendCancel;
    /**
     * Has the request been irrevocably sent?
     */
    private boolean sent;
    private boolean canceled;
    private Scheduler.TaskIdentifier timeoutTask;

    public SimpleRequestIdentifier(T request) {
        this.request = request;
    }

    @Override
    public void retire() {
        if (null != timeoutTask) {
            timeoutTask.cancel();
            timeoutTask = null;
        }
    }

    @Override
    public void setTimeout(final RelativeTime timeout, final TimeoutHandler timeoutHandler) {
        if (null != timeoutTask)
            throw new AssertionError("timeout already set");
        timeoutTask = Scheduler.addDelayed(timeout, new Scheduler.Task() {
            @Override
            public void run(Scheduler.RunContext ctx) {
                if (ctx.reasons.contains(Scheduler.Reason.SHUTDOWN))
                    return;
                timeoutHandler.onTimeout();
            }
        });
    }

    @Override
    public T getRequest() {
        return request;
    }

    @Override
    public void cancel() {
        if (canceled) {
            throw new AssertionError("canceled twice");
        }
        canceled = true;
        if (sent) {
            request.cancel();
        } else if (null != sendCancel) {
            sendCancel.cancel();
            sendCancel = null;
        }
        retire();
    }

    public void send(MessageQueue mq) {
        queued = true;
        Envelope ev = request.assembleRequest();
        ev.notifySent(new NotifySentHandler() {
            @Override
            public void onSent() {
                sendCancel = null;
                sent = true;
            }
        });
        mq.send(ev);
        sendCancel = ev;
    }
}
