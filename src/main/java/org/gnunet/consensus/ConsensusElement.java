/*
 This file is part of GNUnet.
 Copyright (C) 2013 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.consensus;


/**
 * Represents elements of the consensus set.
 */
public class ConsensusElement {
    /**
     * Type of the element.
     * 0 <= elementType <= 2^16
     */
    int elementType;

    /**
     * Data for the element.
     * 0 <= data.length <= 2^16
     */
    public byte[] data;

    /**
     * Create a new element with the given data and given type.
     *
     * @param data data for the element, may not be larger than 2^16 bytes.
     * @param elementType type of the element
     */
    public ConsensusElement(byte[] data, int elementType) {
        this.data = data;
        this.elementType = elementType;
    }
}
