package org.gnunet.testbed;

import org.gnunet.util.Configuration;

/**
 * Opaque handle to a host running experiments managed by the testing framework.
 * The master process must be able to SSH to this host without password (via
 * ssh-agent).
 */
public class Host {
    static int nextUID = 1;
    Configuration cfg;
    final String hostname;
    final String username;
    final int port;

    private boolean controllerStarted;
    public int id;

    /**
     * Create a host to run peers and controllers on.
     *
     * @param hostname name of the host, use "NULL" for localhost
     * @param username username to use for the login; may be NULL
     * @param cfg the configuration to use as a template while starting a controller
     *          on this host.  Operation queue sizes specific to a host are also
     *          read from this configuration handle
     * @param port port number to use for ssh; use 0 to let ssh decide
     */
    public Host(String hostname, String username, Configuration cfg, int port) {
        this.port = (port == 0) ? 22 : port;
        if (hostname == null) {
            id = 0;
        } else {
            id = nextUID++;
        }
        this.hostname = hostname;
        this.username = username;
        this.cfg = cfg;
    }

    /**
     * Create a host handle for the local machine.
     *
     * @param cfg the configuration to use as a template while starting a controller
     *          on this host.  Operation queue sizes specific to a host are also
     *          read from this configuration handle
     * @param port port number to use for ssh; use 0 to let ssh decide
     */
    public Host(Configuration cfg, int port) {
        this(null, null, cfg, port);
    }

    public boolean isLocal() {
        return hostname == null;
    }
}
