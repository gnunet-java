package org.gnunet.testbed;


/**
 * Called on completed operation or error.
 */
public interface OperationCompletionCallback {
    /**
     * Called on completed operation.
     */
    void onCompletion();

    /**
     * Called on error.
     *
     * @param emsg error message
     */
    void onError(String emsg);
}
