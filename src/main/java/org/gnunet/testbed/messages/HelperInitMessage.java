package org.gnunet.testbed.messages;

import org.gnunet.construct.*;
import org.gnunet.util.GnunetMessage;

/**
 * Initialization message for gnunet-helper-testbed to start testbed service
 */
@UnionCase(495)
public class HelperInitMessage implements GnunetMessage.Body {
    /**
     * The controller hostname size excluding the NULL termination character -
     * strlen (hostname); cannot be zero
     */
    @UInt16
    public int trusted_ip_size;
    /**
     * The hostname size excluding the NULL termination character - strlen
     * (hostname); cannot be zero
     */
    @UInt16
    public int hostname_size;
    /**
     * The size of the uncompressed configuration
     */
    @UInt16
    public int config_size;

    @ZeroTerminatedString(optional = true)
    public String trusted_ip;

    @VariableSizeIntegerArray(signed = true, bitSize = 8, lengthField = "hostname_size")
    public byte[] hostname;

    @FillWith @UInt8
    public byte[] compressed_config;
}
