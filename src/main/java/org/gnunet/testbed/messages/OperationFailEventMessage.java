package org.gnunet.testbed.messages;

import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;


@UnionCase(473)
public class OperationFailEventMessage implements GnunetMessage.Body {
    @UInt32
    public int eventType;
    @UInt64
    public long operationId;
    @ZeroTerminatedString
    public String errorMessage;
}
