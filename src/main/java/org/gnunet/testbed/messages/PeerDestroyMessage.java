package org.gnunet.testbed.messages;


import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

@UnionCase(468)
public class PeerDestroyMessage implements GnunetMessage.Body {
    @UInt32
    public int peerId;
    @UInt64
    public long operationId;
}
