package org.gnunet.testbed.messages;


import org.gnunet.construct.*;
import org.gnunet.util.GnunetMessage;

@UnionCase(483)
public class ManagePeerServiceMessage implements GnunetMessage.Body {
    /**
     * Testbed-internal ID of the target peer.
     */
    @UInt32
    public int peerId;

    /**
     * Operation ID.
     */
    @UInt64
    public long operationId;

    @UInt8
    public boolean start;

    /**
     * Name of the service to start or stop.
     */
    @ZeroTerminatedString(optional = false)
    public String serviceName;
}
