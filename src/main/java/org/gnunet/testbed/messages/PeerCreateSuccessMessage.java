package org.gnunet.testbed.messages;

import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * Event notification from a controller to a client.
 */
@UnionCase(474)
public class PeerCreateSuccessMessage implements GnunetMessage.Body {
    @UInt32
    public int peerId;
    @UInt64
    public long operationId;
}
