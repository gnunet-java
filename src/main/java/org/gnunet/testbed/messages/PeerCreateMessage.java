package org.gnunet.testbed.messages;


import org.gnunet.construct.*;
import org.gnunet.util.GnunetMessage;

@UnionCase(464)
public class PeerCreateMessage implements GnunetMessage.Body {
    @UInt32
    public int hostId;
    @UInt64
    public long operationId;
    @UInt32
    public int peerId;

    /**
     * The size of the uncompressed configuration
     */
    @UInt16
    public int configSize;

    @FillWith @UInt8
    public byte[] compressedConfig;
}
