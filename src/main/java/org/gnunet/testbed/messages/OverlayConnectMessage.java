package org.gnunet.testbed.messages;

import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * Message sent from client to testing service to
 * connect two peers.
 */
@UnionCase(470)
public class OverlayConnectMessage implements GnunetMessage.Body {
    /**
     * Unique ID for the first peer.
     */
    @UInt32
    public int peer1;

    /**
     * Operation ID that is used to identify this operation.
     */
    @UInt64
    public long operationId;

    /**
     * Unique ID for the second peer.
     */
    @UInt32
    public int peer2;

    /**
     * The ID of the host which runs peer2
     */
    @UInt32
    public int hostOfPeer2;
}
