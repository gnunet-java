package org.gnunet.testbed.messages;

import org.gnunet.construct.*;
import org.gnunet.util.GnunetMessage;

/**
 * Message sent from client to testing service to
 * reconfigure a (stopped) a peer.
 */
@UnionCase(465)
public class PeerReconfigureMessage implements GnunetMessage.Body {
    @UInt32
    public int peerId;
    @UInt64
    public long operationId;
    @UInt16
    public int uncompressedConfigSize;
    @FillWith
    @UInt8
    public byte[] compressedConfig;
}
