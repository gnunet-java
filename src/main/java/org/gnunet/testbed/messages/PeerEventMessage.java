package org.gnunet.testbed.messages;


import org.gnunet.construct.Int32;
import org.gnunet.construct.Int64;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * Event notification from a controller to a client.
 */
@UnionCase(471)
public class PeerEventMessage implements GnunetMessage.Body {
    /**
     * 'enum GNUNET_TESTBED_EventType'
     * either GNUNET_TESTBED_ET_PEER_START or GNUNET_TESTBED_ET_PEER_STOP.
     */
    @Int32
    public int eventType;
    @Int32
    public int host_id;
    @Int32
    public int peer_id;
    @Int64
    public long operationId;
}
