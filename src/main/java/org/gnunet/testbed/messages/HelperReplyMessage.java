package org.gnunet.testbed.messages;


import org.gnunet.construct.FillWith;
import org.gnunet.construct.UInt16;
import org.gnunet.construct.UInt8;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

@UnionCase(496)
public class HelperReplyMessage implements GnunetMessage.Body {
    @UInt16
    public int uncompressed_config_size;

    @FillWith @UInt8
    public byte[] compressed_config;
}
