/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.testbed.messages;

import org.gnunet.construct.*;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;

@UnionCase(477)
public class PeerInformationMessage implements GnunetMessage.Body {
    /**
     * The testbed-internal id of the peer relevant to this
     * information.
     */
    @UInt32
    public int peerId;

    /**
     * Operation ID of the operation that created this event.
     */
    @UInt64
    public long operationId;

    /**
     * Identity of the peer.
     */
    @NestedMessage
    public PeerIdentity peerIdentity;

    @UInt16
    public int uncompressedConfigSize;

    @FillWith @UInt8
    public byte[] compressedConfig;
}
