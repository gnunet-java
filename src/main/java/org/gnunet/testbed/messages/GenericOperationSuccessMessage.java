package org.gnunet.testbed.messages;

import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.util.GnunetMessage;

/**
 * Event notification from a controller to a client for
 * a generic operational success where the operation does
 * not return any data.
 */
@UnionCase(475)
public class GenericOperationSuccessMessage implements GnunetMessage.Body {
    @UInt32
    public int eventType;
    @UInt64
    public long operationId;
}
