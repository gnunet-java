/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */
package org.gnunet.testbed.messages;


import org.gnunet.construct.UInt32;
import org.gnunet.construct.UInt64;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;

/**
 * Initial message from a client to a testbed control service.
 */
@UnionCase(460)
public class ControllerInitMessage implements GnunetMessage.Body {
    /**
     * Host ID that the controller is either given (if this is the
     * dominating client) or assumed to have (for peer-connections
     * between controllers).  A controller must check that all
     * connections make consistent claims...
     */
    @UInt32
    public int hostId;

    /**
     * Event mask that specifies which events this client is interested in.
     */
    @UInt64
    public long eventMask;

    /**
     * 0-terminated hostname of the controller.
     */
    @ZeroTerminatedString
    public String controlerHostname;

}
