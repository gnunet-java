package org.gnunet.testbed;


import org.gnunet.util.Configuration;

/**
 * Listener for controller status changes.
 */
public interface ControllerStatusCallback {
    /**
     * Called on successful startup.
     *
     * @param cfg configuration
     */
    void onStartupSuccess(Configuration cfg);

    /**
     * Called on failed startup.
     */
    void onStartupFailure();
}
