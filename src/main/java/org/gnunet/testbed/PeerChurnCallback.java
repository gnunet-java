package org.gnunet.testbed;


public interface PeerChurnCallback {
    void onChurnSuccess();
    void onChurnError(String emsg);
}
