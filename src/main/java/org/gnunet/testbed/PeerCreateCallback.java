package org.gnunet.testbed;

import org.gnunet.testbed.Controller;

public interface PeerCreateCallback {
    void onPeerCreated(Controller.Peer peer);
    void onError(String errorMessage);
}
