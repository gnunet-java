#!/bin/bash

# stop on non-zero exit value
set -e
# stop on using unset variable
set -u

# get the dir where our script runs from
SDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# friendly reminder to have gnunet-java in path
if [[ -z $(which gnunet-ballot) ]]; then
  echo "no gnunet-ballot found, did you set PATH correctly?" 1>&2
  exit 1
fi


# prefix for testing etc.
PREFIX=$(mktemp -d test-voting-XXXXXXXXXX.d --tmpdir)

echo "I'm in '$SDIR'"
echo "prefix: $PREFIX"

# authority config
CFG0="$PREFIX/0/config"

CLEAN=0
function cleanup {
  if [[ $CLEAN == 0 ]]; then
    CLEAN=1
    echo $1
    kill $(jobs -p) &>/dev/null
    sleep 5 && kill -9 $(jobs -p) &>/dev/null &
    KILLER=$!
    for PID in $(jobs -p); do
      kill -0 $PID &>/dev/null
      ALIVE=$?
      if [[ $ALIVE && $PID != $KILLER ]]; then
        wait $PID &>/dev/null
      fi
    done
    kill $KILLER
  fi
  exit 1
}

trap 'cleanup "error in line $LINENO"' ERR
trap "cleanup \"got sigint\"" SIGINT
trap "cleanup \"exiting\"" EXIT

export GNUNET_FORCE_LOG=";;;;ERROR"
#export GNUNET_FORCE_LOG="cadet;;;;DEBUG"
export GNUNET_TESTING_PREFIX=$PREFIX
gnunet-testbed-profiler -n -c "$SDIR/test_voting.conf" -p 1 &

gnunet-ballot -t "$PREFIX/ballot"

gnunet-config -c "$PREFIX/ballot" -s election -o TOPIC -V mytopic

PRIVKFILE=$(gnunet-config -f -c $CFG0 -s peer -o private_key)
echo privkeyfile $PRIVKFILE

PUBK=$(gnunet-ecc --print-public-key $PRIVKFILE)
echo pubkey $PUBK

PEERID=$(gnunet-ecc --print-peer-identity $PRIVKFILE)
echo peerid $PEERID

gnunet-config -c "$PREFIX/ballot" -s authorities -o auth1 -V "$PEERID"

gnunet-identity -c $CFG0 -C ca
gnunet-identity -c $CFG0 -C voter0
gnunet-identity -c $CFG0 -C issuer

function pubget {
  gnunet-identity -c $CFG0 -d | grep "^$1 -" | sed 's/.*- *//g'
}

CA_PUB=$(pubget ca)
V0_PUB=$(pubget voter0)

echo V0_PUB $V0_PUB

if [[ -z $CA_PUB ]]; then
  echo "could not generate ca pubkey"
  exit 1
fi

gnunet-config -c "$PREFIX/ballot" -s election -o CA_PUB -V "$CA_PUB"

gnunet-config -c "$PREFIX/ballot" -s election -o GROUP -V "test voters"


NOW=$(date +%s)

declare -i TS_START=NOW+15
declare -i TS_CLOSING=TS_START+5
declare -i TS_CONCLUDE=TS_START+10
declare -i TS_QUERY=TS_START+20
declare -i TS_END=TS_START+500



function settime {
  local VAR="TS_$1"
  gnunet-config -c "$PREFIX/ballot" -s election -o "TIMESTAMP_$1" -V ${!VAR}
}

settime START
settime CLOSING
settime CONCLUDE
settime QUERY
settime END

gnunet-ballot-group-certify -c $CFG0 -g "test voters" -e ca -m $V0_PUB > "$PREFIX/v0-cert"

gnunet-ballot -c $CFG0 -i -e issuer "$PREFIX/ballot"

# FIXME: daemon should be started with arm
gnunet-daemon-ballot-tally -c $CFG0 &

sleep 5

gnunet-ballot -c $CFG0 -r "$PREFIX/ballot"

gnunet-ballot -c $CFG0 -g "$PREFIX/v0-cert" "$PREFIX/ballot"

gnunet-ballot -c $CFG0 -x yes "$PREFIX/ballot" -e voter0

NOW=$(date +%s)

if [[ $NOW -lt $TS_START ]]; then
  T=$((TS_START - NOW))
  echo sleeping $T s until start
  sleep $((TS_START - NOW))
fi

if [[ $NOW -ge $TS_CLOSING ]]; then
  echo "we are to slow, ballot already closed"
  echo "now: $NOW, closing: $TS_CLOSING"
  exit 1
fi


gnunet-ballot -c $CFG0 -s "$PREFIX/ballot"

NOW=$(date +%s)

if [[ $NOW -lt $TS_QUERY ]]; then
  T=$((TS_QUERY - NOW))
  echo sleeping $T s until query
  sleep $((TS_QUERY - NOW))
fi

gnunet-ballot -c $CFG0 -q "$PREFIX/ballot"

# check validity
#gnunet-ballot -V "$PREFIX/ballot"


