/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.grothoff;


public class RunaboutBenchmark {

    public static class MyRunabout extends Runabout {
        public void visit(String s) {

        }
        public void visit(Integer i) {

        }
    }


    public static void main(String[] args) {
        final int runs = 5000000;

        Runabout r = new MyRunabout();

        long start = System.currentTimeMillis();

        Integer integer = 42;
        for (int i = 0; i < runs; ++i) {
            r.visitAppropriate("foo");
            r.visitAppropriate(integer);
        }

        long end = System.currentTimeMillis();

        long duration1 = end - start;


        Runabout r2 = new Runabout() {
            public void visit(String s) {

            }
            public void visit(Integer i) {

            }
        };


        start = System.currentTimeMillis();

        for (int i = 0; i < runs; ++i) {
            r2.visitAppropriate("foo");
            r2.visitAppropriate(integer);
        }

        end = System.currentTimeMillis();

        long duration2 = end - start;


        System.out.println("Runs: " + runs);
        System.out.println("public: " + duration1);
        System.out.println("Anon Inner Class: " + duration2);
        System.out.println("Overhead: " + duration2 / (double) duration1);

    }
}
