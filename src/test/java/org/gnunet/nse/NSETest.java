/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.nse;

import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.AbsoluteTime;
import org.gnunet.util.Scheduler;
import org.gnunet.util.Wrapper;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Florian Dold
 */
public class NSETest {
    @Test
    public void test_nse() {
        final Wrapper<Boolean> gotResult = new Wrapper<Boolean>(false);
        TestingSubsystem ts = new TestingSubsystem("nse");

        final NetworkSizeEstimation nse = new NetworkSizeEstimation(ts.getConfiguration());
        nse.subscribe(new NetworkSizeEstimation.Subscriber() {
            @Override
            public void update(AbsoluteTime timestamp, double estimate, double deviation) {
                assertNotNull(timestamp);
                gotResult.set(true);
                nse.disconnect();
            }
        });

        Scheduler.run();

        assertTrue(gotResult.get());
    }
}
