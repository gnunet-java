/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.consensus;

import org.gnunet.testing.TestingFixture;
import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Test consensus with only one peer.
 */
public class ConsensusSingleTest extends TestingFixture {
    @Test
    public void test_consensus_single() {
        Configuration armConf = new Configuration();
        armConf.setValueString("arm", "DEFAULTSERVICES", "consensus set");
        Program.configureLogging("DEBUG");

        TestingSubsystem ts = new TestingSubsystem("arm", armConf.writeTemp().getAbsolutePath());
        final Wrapper<Boolean> isDone = new Wrapper<Boolean>(false);

        final Consensus consensus = new Consensus(
                ts.getConfiguration(),
                new PeerIdentity[0],
                HashCode.random(),
                AbsoluteTime.now(),
                AbsoluteTime.now().add(RelativeTime.SECOND.multiply(10)));

        consensus.insertElement(new ConsensusElement("foo".getBytes(), 0));
        consensus.insertElement(new ConsensusElement("bar".getBytes(), 0));
        consensus.insertElement(new ConsensusElement("baz".getBytes(), 0));

        final List<String> received = new LinkedList<String>();

        consensus.conclude(new ConsensusCallback() {
            @Override
            public void onElement(ConsensusElement element) {
                Assert.assertNotNull(element);
                received.add(new String(element.data));
            }

            @Override
            public void onDone() {
                consensus.destroy();
                isDone.set(true);
            }
        });

        Scheduler.run();
        Assert.assertTrue(isDone.get());
        Assert.assertEquals(3, received.size());
        Assert.assertTrue(received.contains("foo"));
        Assert.assertTrue(received.contains("bar"));
        Assert.assertTrue(received.contains("baz"));
    }
}
