/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.core;


import org.gnunet.testing.TestingFixture;
import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.*;
import org.grothoff.Runabout;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CoreTest extends TestingFixture {
    @Test(timeout = 10000)
    public void test_core_init() {
        Program.configureLogging("DEBUG");
        TestingSubsystem ts = new TestingSubsystem("core");

        final Wrapper<Boolean> res = new Wrapper<Boolean>(false);

        final Core core = new Core(ts.getConfiguration());
        core.observeConnect(new ConnectHandler() {
            @Override
            public void onConnect(PeerIdentity peerIdentity) {
            }
        });
        core.init(new InitCallback() {
            @Override
            public void onInit(PeerIdentity myIdentity) {
                res.value = true;
                System.out.println("in core init");
                assertTrue(myIdentity != null);
                core.disconnect();
            }
        });

        Scheduler.run();

        ts.destroy();

        assertTrue(res.value);
    }


    @Test(timeout = 10000)
    public void test_core_echo() {
        Program.configureLogging("DEBUG");
        new TestingSubsystem("core");
        new TestingSubsystem("core");
        new TestingSubsystem("core");
        new TestingSubsystem("core");
        TestingSubsystem ts = new TestingSubsystem("core");

        final Wrapper<Boolean> gotResponse = new Wrapper<Boolean>(false);

        final Core core = new Core(ts.getConfiguration());
        core.setMessageHandler(new Runabout() {
            public void visit(TestMessage t) {
                System.out.println("got core response");
                gotResponse.set(true);
                core.disconnect();
            }
        });

        core.init(new InitCallback() {
            @Override
            public void onInit(PeerIdentity myIdentity) {
                System.out.println("in core init, my identity is " + myIdentity);
                // we want to send a 4-byte TestMessage
                core.notifyTransmitReady(0, RelativeTime.FOREVER, myIdentity, 4, new MessageTransmitter() {
                    @Override
                    public void transmit(Connection.MessageSink sink) {
                        System.out.println("ntr called, calling send");
                        sink.send(new TestMessage());
                    }

                    @Override
                    public void handleError() {
                        Assert.fail();
                    }
                });
            }
        });

        Scheduler.run();

        ts.destroy();

        assertTrue(gotResponse.get());

    }
}
