/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.testing;

import org.gnunet.util.Program;
import org.junit.Test;

/**
 * @author Florian Dold
 */
public class TestingSetupTest {
    static final String service = "nse";

    @Test(timeout = 1000)
    public void test_testing() {
        Program.configureLogging();
        // could be any service, just use statistics
        TestingSubsystem ts = new TestingSubsystem(service);
        String port = ts.getConfiguration().getValueString(service, "PORT").get();
        org.junit.Assert.assertTrue(port != null);

        ts.destroy();
    }

    @Test(expected = TestingSetup.SetupException.class)
    public void test_no_service() {
        new TestingSubsystem("foobar _ !!!");
    }

    @Test(timeout = 1000)
    public void test_restart() {
        TestingSubsystem ts = new TestingSubsystem(service);
        ts.restart();
        ts.destroy();
    }
}
