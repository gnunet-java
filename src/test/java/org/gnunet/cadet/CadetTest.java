package org.gnunet.cadet;

import org.gnunet.core.Core;
import org.gnunet.core.PeerIdentityContinuation;
import org.gnunet.testing.TestingFixture;
import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.*;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class CadetTest extends TestingFixture {
    public static class MessageHandler1 extends CadetRunabout {
        public Cadet m1;
        public Cadet m2;
        public void visit(TestMessage m) {
            System.out.println("got test message");
            m1.destroy();
            m2.destroy();
        }
    }

    public static class MyChannelEndHandler implements ChannelEndHandler {
        Cadet cadet1;
        PeerIdentity peerIdentity;
        @Override
        public void onChannelEnd(Cadet.Channel channel) {
            Cadet.Channel myChannel = cadet1.createChannel(peerIdentity, 42, false, true);
            myChannel.send(new TestMessage());
        }
    }

    /**
     * Send a message locally, to our own peer.
     */
    @Test (timeout = 5000)
    public void test_cadet_send() {
        Program.configureLogging("DEBUG");
        // we want the full arm here (starts both cadet and core)
        final TestingSubsystem ts = new TestingSubsystem("arm");
        final Configuration cfg = ts.getConfiguration();

        final MessageHandler1 mh = new MessageHandler1();
        // FIXME: use CryptoEcc instead of Core once available and compatible
        Core.withPeerIdentity(cfg, new PeerIdentityContinuation() {
            @Override
            public void cont(PeerIdentity peerIdentity) {
                System.out.println("got peer identity from core");
                //
                MyChannelEndHandler teh = new MyChannelEndHandler();
                // first cadet handle, used to send the message
                final Cadet cadet1 = new Cadet(cfg, teh);
                teh.cadet1 = cadet1;
                teh.peerIdentity = peerIdentity;
                // second cadet handle, used to receive the message
                final Cadet cadet2 = new Cadet(cfg, new InboundChannelHandler() {
                    @Override
                    public void onInboundChannel(Cadet.Channel tunnel, PeerIdentity initiator) {
                        System.out.println("got inbound channel " + initiator);
                    }
                }, new ChannelEndHandler() {
                    @Override
                    public void onChannelEnd(Cadet.Channel tunnel) {
                        System.out.println("channel end handler called");
                    }
                }, mh, 42);
                mh.m1 = cadet1;
                mh.m2 = cadet2;
                Cadet.Channel channel = cadet1.createChannel(peerIdentity, 42, false, true);
                channel.send(new TestMessage());
            }
        });
        Scheduler.run();
    }
}
