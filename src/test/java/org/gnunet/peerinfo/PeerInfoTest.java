package org.gnunet.peerinfo;

import org.gnunet.hello.HelloMessage;
import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.*;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * ...
 *
 * @author Florian Dold
 */
public class PeerInfoTest {

    @Test
    public void test_peerinfo() {
        Program.configureLogging("debug");
        final Wrapper<Boolean> ended = new Wrapper<Boolean>(false);
        TestingSubsystem ts = new TestingSubsystem("peerinfo");
        final PeerInfo peerInfo = new PeerInfo(ts.getConfiguration());

        peerInfo.iterate(RelativeTime.FOREVER, false, new PeerProcessor() {
            @Override
            public void onPeer(PeerIdentity peerIdentity, HelloMessage hello) {
                // we can't expect to get anything here, peerinfo is the only running service in this setup
            }

            @Override
            public void onEnd() {
                peerInfo.disconnect();
                ended.set(true);
            }
        });

        Scheduler.run();
        assertTrue(ended.get());
    }
}
