/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.identity;


import org.gnunet.testing.TestingFixture;
import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.Program;
import org.gnunet.util.Scheduler;
import org.gnunet.util.Wrapper;
import org.junit.Assert;
import org.junit.Test;

public class IdentityTest extends TestingFixture {
    @Test
    public void test_identity_connect() {
        final Wrapper<Boolean> reachedEnd = new Wrapper<Boolean>(false);
        Program.configureLogging("DEBUG");
        TestingSubsystem ts = new TestingSubsystem("identity");

        final Identity identity = new Identity();
        identity.connect(ts.getConfiguration(), new IdentityListCallback() {
            @Override
            public void onEgoAdd(Identity.Ego ego) {
                System.out.println("got ego " + ego.getName());
            }

            @Override
            public void onEgoDelete(Identity.Ego ego) {
                // should only happen after end of list,
                // but we disconnect on end of list
                Assert.fail();
            }

            @Override
            public void onEgoRename(String oldName, Identity.Ego ego) {
                Assert.fail();
            }

            @Override
            public void onListEnd() {
                System.out.println("got end of list");
                reachedEnd.set(true);
                identity.disconnect();
            }
        });
        Scheduler.run();
        Assert.assertTrue(reachedEnd.get());
    }

    @Test
    public void test_identity_create() {
        final Wrapper<Boolean> created = new Wrapper<Boolean>(false);
        final Wrapper<Boolean> gotEnd = new Wrapper<Boolean>(false);
        final Wrapper<Boolean> gotCreated = new Wrapper<Boolean>(false);
        Program.configureLogging("DEBUG");
        TestingSubsystem ts = new TestingSubsystem("identity");

        final String myEgoName = "gnunet-java-test-ego";

        final Identity identity = new Identity();
        identity.connect(ts.getConfiguration(), new IdentityListCallback() {
            @Override
            public void onEgoAdd(Identity.Ego ego) {
                if (created.get() && ego.getName().equals(myEgoName)) {
                    gotCreated.set(true);
                    identity.disconnect();
                }
            }

            @Override
            public void onEgoDelete(Identity.Ego ego) {
                // should only happen after end of list,
                // but we disconnect on end of list
                Assert.fail();
            }

            @Override
            public void onEgoRename(String oldName, Identity.Ego ego) {
                Assert.fail();
            }

            @Override
            public void onListEnd() {
                System.out.println("got end of list");
                gotEnd.set(true);
            }
        });
        identity.create(myEgoName, new IdentityContinuation() {
            @Override
            public void onError(String errorMessage) {
                Assert.fail(errorMessage);
            }

            @Override
            public void onDone() {
                Assert.assertFalse(created.get());
                created.set(true);
            }
        });
        Scheduler.run();
        Assert.assertTrue(created.get());
        Assert.assertTrue(gotCreated.get());
        Assert.assertTrue(gotEnd.get());
    }
}
