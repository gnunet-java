/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.transport;

import org.gnunet.hello.HelloMessage;
import org.gnunet.testing.TestingFixture;
import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.Program;
import org.gnunet.util.Scheduler;
import org.gnunet.util.Wrapper;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TransportTest extends TestingFixture {
    @Test(timeout = 5000)
    public void test_transport_get_hello() {
        Program.configureLogging("debug");
        final Wrapper<Boolean> ended = new Wrapper<Boolean>(false);
        TestingSubsystem ts = new TestingSubsystem("transport");
        final Transport transport = new Transport(ts.getConfiguration());
        transport.getHello(new HelloUpdateCallback() {
            @Override
            public void onHello(HelloMessage helloMessage) {
                ended.set(true);
                transport.disconnect();
            }
        });

        Scheduler.run();
        assertTrue(ended.get());
    }
}
