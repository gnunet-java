package org.gnunet.construct;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class FixedSizeTest {

    public static class Msg implements Message {
        @UInt8
        public int v;

        public Msg() {
            // default ctor required by Construct
        }

        public Msg(int v) {
            this.v = v;
        }
    }

    public static class FixedSizeTestMessage implements Message {
        @FixedSizeArray(length = 4)
        public Msg[] msgs;
    }


    @Test
    public void test_fixedNested() {
        FixedSizeTestMessage m = new FixedSizeTestMessage();
        m.msgs = new Msg[]{new Msg(1), new Msg(2), new Msg(3), new Msg(4)};
        byte[] bytes = Construct.toBinary(m);

        FixedSizeTestMessage m2 = Construct.parseAs(bytes, FixedSizeTestMessage.class);

        Assert.assertEquals(m.msgs[0].v, m2.msgs[0].v);
        Assert.assertEquals(m.msgs[1].v, m2.msgs[1].v);
        Assert.assertEquals(m.msgs[2].v, m2.msgs[2].v);
        Assert.assertEquals(m.msgs[3].v, m2.msgs[3].v);
    }

    @Test(expected = AssertionError.class)
    public void test_sizeMismatch() {
        FixedSizeTestMessage m = new FixedSizeTestMessage();
        m.msgs = new Msg[]{new Msg(1), new Msg(2)};
        Construct.toBinary(m);
    }
}
