package org.gnunet.construct;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class DoubleTest {
    public static class DoubleMessage implements Message {
        @DoubleValue
        public double d1;
        @DoubleValue
        public double d2;
    }

    @Test
    public void test_double() {
        DoubleMessage m = new DoubleMessage();
        m.d1 = 1.123;
        m.d2 = java.lang.Double.NaN;

        byte[] data = Construct.toBinary(m);

        DoubleMessage m2 = Construct.parseAs(data, DoubleMessage.class);

        Assert.assertEquals(m.d1, m2.d1, 0);
        Assert.assertEquals(m.d2, m2.d2, 0);

        Assert.assertEquals(8+8, data.length);
    }

}
