package org.gnunet.construct;

import com.google.common.base.Objects;

/**
* Message to test string parsing in construct.
*
* @author Florian Dold
*/
public class StringTuple implements Message {
    @ZeroTerminatedString
    public String str1;
    @ZeroTerminatedString
    public String str2;

    public StringTuple() {
        // empty default ctor needed by Construct
    }
    public StringTuple(String str1, String str2) {
        this.str1 = str1;
        this.str2 = str2;
    }
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof StringTuple)) {
            return false;
        }
        StringTuple otherT = (StringTuple) other;
        return otherT.str1.equals(this.str1) && otherT.str2.equals(this.str2);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(str1, str2);
    }
}
