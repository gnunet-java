package org.gnunet.construct;

/**
 * ...
 *
 * @author Florian Dold
 */
public class IntMessage implements Message {
    @UInt64
    public long i1;
    @UInt32
    public long i2;
    @UInt16
    public long i3;
    @UInt8
    public long i4;
    @Int64
    public long i5;
    @Int32
    public long i6;
    @Int16
    public long i7;
    @Int8
    public long i8;
}
