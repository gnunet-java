package org.gnunet.construct;

import org.gnunet.core.messages.SendMessage;
import org.gnunet.util.AbsoluteTime;
import org.gnunet.util.GnunetMessage;
import org.gnunet.util.PeerIdentity;
import org.gnunet.util.TestMessage;
import org.junit.Test;

/**
 * Regression test for a message class in org.gnunet.core
 *
 * todo: should this test be really here?
 *
 * @author Florian Dold
 */
public class SendMessageTest {

    @Test
    public void test_patch() {
        SendMessage m = new SendMessage();
        m.deadline = AbsoluteTime.FOREVER.asMessage();
        m.peer = new PeerIdentity(); // null identity
        m.payloadMessage = new GnunetMessage();
        m.payloadMessage.body = new TestMessage();
        m.payloadMessage.header = new GnunetMessage.Header();

        GnunetMessage container = new GnunetMessage();
        container.body = m;
        container.header = new GnunetMessage.Header();

        Construct.patch(container);
    }
}
