package org.gnunet.construct;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class VariableSizeArrayTest {
    public static class VariableTestMessage implements Message {
        @UInt32
        public int num;
        @VariableSizeArray(lengthField = "num")
        public StringTuple[] msgs;
    }

    @Test
    public void test_variableSizeArray() {
        VariableTestMessage m = new VariableTestMessage();
        m.msgs = new StringTuple[]{new StringTuple("foo", "bar"), new StringTuple("quux", "baz"), new StringTuple("spam", "eggs")};
        Construct.patch(m);
        Assert.assertEquals(3, m.num);
        byte[] data = Construct.toBinary(m);
        VariableTestMessage m2 = Construct.parseAs(data, VariableTestMessage.class);
        Assert.assertEquals(m2.num, 3);
        Assert.assertEquals(m.msgs[0], m2.msgs[0]);
        Assert.assertEquals(m.msgs[1], m2.msgs[1]);
        Assert.assertEquals(m.msgs[2], m2.msgs[2]);

    }
}
