package org.gnunet.construct;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class FillParserTest {

    public static class FillTestMessage implements Message {
        @FrameSize
        @UInt32
        public int size;
        @FillWith
        public StringTuple[] strings;
    }

    @Test
    public void test_fillParser() {
        FillTestMessage m = new FillTestMessage();
        m.strings = new StringTuple[]{new StringTuple("foo", "bar"), new StringTuple("quux", "spam")};
        Construct.patch(m);
        System.out.println(m.size);
        byte[] data = Construct.toBinary(m);
        Assert.assertEquals(m.size, data.length);

        FillTestMessage m2 = Construct.parseAs(data, FillTestMessage.class);

        Assert.assertEquals(m.strings.length, m2.strings.length);

        Assert.assertEquals(m.strings[0], m2.strings[0]);
        Assert.assertEquals(m.strings[1], m2.strings[1]);
    }
}
