/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.construct;

import org.junit.Assert;
import org.junit.Test;

public class VariableStringTest {
    public static class VariableStringMessage implements Message {
        @UInt32
        public int len1;
        @UInt32
        public int len2;
        @UInt32
        public int len3;
        @VariableSizeString(lengthField = "len1", terminationType = StringTerminationType.NONE)
        public String str1;
        @VariableSizeString(lengthField = "len2", terminationType = StringTerminationType.ZERO_EXCLUDED)
        public String str2;
        @VariableSizeString(lengthField = "len3", terminationType = StringTerminationType.ZERO_INCLUDED)
        public String str3;
        @UInt8
        public int sentinel;
    }

    @Test
    public void test_variable_string() {
        VariableStringMessage m = new VariableStringMessage();
        m.str1 = "foo";
        m.str2 = "quux";
        m.str3 = "42";
        m.sentinel = 123;
        Construct.patch(m);
        Assert.assertEquals(3, m.len1);
        Assert.assertEquals(4, m.len2);
        Assert.assertEquals(3, m.len3);
        byte[] data = Construct.toBinary(m);
        Assert.assertEquals(3+5+3+3*4+1, data.length);
        VariableStringMessage m2 = Construct.parseAs(data, VariableStringMessage.class);
        Assert.assertEquals("foo", m2.str1);
        Assert.assertEquals("quux", m2.str2);
        Assert.assertEquals("42", m2.str3);
        Assert.assertEquals(123, m2.sentinel);
    }
}
