package org.gnunet.construct;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class FrameSizeTest {
    public static class CoordMessage implements Message {
        @FrameSize
        @UInt32
        public int size;
        @UInt32
        public int x;
        @UInt8
        public int y;
    }

    public static class RecursiveMessage implements Message {
        @FrameSize
        @UInt32
        public int size;

        @ZeroTerminatedString
        public String data;

        @NestedMessage(newFrame = true, optional = true)
        public RecursiveMessage rec;

    }

    @Test
    public void test_simple() {
        CoordMessage m = new CoordMessage();
        Construct.patch(m);
        Assert.assertEquals(9, m.size);
    }


    //@Test
    public void test_recursive_1() {
        RecursiveMessage rm = new RecursiveMessage();
        rm.data = "foo";
        Construct.patch(rm);
    }
}
