package org.gnunet.construct;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class StringTest {
    public static class StrMsg implements Message {
        @FrameSize
        @UInt32
        public int len;
        @ZeroTerminatedString(optional = false)
        public String str1;
        @ZeroTerminatedString(optional = true)
        public String str2;
    }


    @Test
    public void test_empty() {
        StrMsg m = new StrMsg();
        m.str1 = "";
        m.str2 = "";
        Construct.patch(m);
        byte[] data = Construct.toBinary(m);
        Assert.assertEquals(4+1+1, data.length);
        StrMsg m2 = Construct.parseAs(data, StrMsg.class);
        Assert.assertEquals("", m2.str1);
        Assert.assertEquals("", m2.str2);
    }

    @Test
    public void test_null() {
        StrMsg m = new StrMsg();
        m.str1 = "";
        m.str2 = null;
        Construct.patch(m);
        byte[] data = Construct.toBinary(m);
        Assert.assertEquals(4+1, data.length);
        Assert.assertEquals(4+1, m.len);
        StrMsg m2 = Construct.parseAs(data, StrMsg.class);
        Assert.assertEquals("", m2.str1);
        Assert.assertEquals(null, m2.str2);
    }
}

