package org.gnunet.construct;

/**
 * ...
 *
 * @author Florian Dold
 */
public class PrivateMemberMessage implements Message {
    @UInt32
    public int foo;
    @UInt32
    private int bar;
}
