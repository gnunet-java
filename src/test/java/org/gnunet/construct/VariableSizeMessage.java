package org.gnunet.construct;

/**
 * ...
 *
 * @author Florian Dold
 */
public class VariableSizeMessage implements Message {
    @UInt16
    public int n1;
    @VariableSizeIntegerArray(lengthField = "n1", signed = false, bitSize = 16)
    public int[] a1;
    @UInt16
    public int n2;
    @VariableSizeIntegerArray(lengthField = "n2", signed = false, bitSize = 16)
    public int[] a2;
}
