/*
 This file is part of GNUnet.
 Copyright (C) 2014 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.secretsharing;

import org.gnunet.testing.TestingSubsystem;
import org.gnunet.util.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.math.BigInteger;

public class SecretsharingTest {
    @Test
    public void test_secretsharing_single_generate() {
        Program.configureLogging("debug");
        Configuration armConf = new Configuration();
        armConf.setValueString("arm", "DEFAULTSERVICES", "consensus set secretsharing");
        Program.configureLogging("DEBUG");
        TestingSubsystem ts = new TestingSubsystem("arm", armConf.writeTemp().getAbsolutePath());
        final Wrapper<Boolean> success = new Wrapper<Boolean>(false);
        final KeyGeneration keyGeneration = new KeyGeneration(
                ts.getConfiguration(),
                new PeerIdentity[0],
                HashCode.random(),
                AbsoluteTime.now(),
                AbsoluteTime.now().add(RelativeTime.SECOND),
                1,
                new SecretReadyCallback() {
                    @Override
                    public void onSecretReady(Share share) {
                        if (share != null && share.numPeers == 1) {
                            success.set(true);
                        }
                        System.out.println("got secret share");
                    }
                });
        Scheduler.run();
        Assert.assertTrue(success.get());
    }



    @Test
    public void test_secretsharing_single_decrypt() {
        Program.configureLogging("debug");
        Configuration armConf = new Configuration();
        armConf.setValueString("arm", "DEFAULTSERVICES", "consensus set secretsharing");
        Program.configureLogging("DEBUG");
        final TestingSubsystem ts = new TestingSubsystem("arm", armConf.writeTemp().getAbsolutePath());
        final Wrapper<Boolean> success = new Wrapper<Boolean>(false);
        final Plaintext p = Plaintext.generate(new BigInteger("42"));
        final KeyGeneration keyGeneration = new KeyGeneration(
                ts.getConfiguration(),
                new PeerIdentity[0],
                HashCode.random(),
                AbsoluteTime.now(),
                AbsoluteTime.now().add(RelativeTime.SECOND),
                1,
                new SecretReadyCallback() {
                    @Override
                    public void onSecretReady(Share share) {
                        final Ciphertext c = p.encrypt(share.publicKey);
                        Decryption decryption = new Decryption(
                                ts.getConfiguration(),
                                share,
                                c,
                                AbsoluteTime.now(),
                                AbsoluteTime.now().add(RelativeTime.SECOND),
                                new DecryptCallback() {
                                    @Override
                                    public void onResult(Plaintext resultPlaintext) {
                                        success.set(true);
                                        Assert.assertArrayEquals(p.bits, resultPlaintext.bits);
                                    }
                                });


                        BigInteger s = new BigInteger(1, share.myShare.bits);
                        BigInteger h = new BigInteger(1, share.publicKey.bits);
                        Assert.assertEquals(h, Parameters.elgamalG.modPow(s, Parameters.elgamalP));
                        System.out.println("got secret share");
                    }
                });
        Scheduler.run();
        Assert.assertTrue(success.get());
    }
}
