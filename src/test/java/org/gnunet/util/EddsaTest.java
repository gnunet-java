/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;


import org.gnunet.util.crypto.EddsaPrivateKey;
import org.gnunet.util.crypto.EddsaPublicKey;
import org.gnunet.util.crypto.EddsaSignature;
import org.junit.Assert;
import org.junit.Test;

public class EddsaTest {
    @Test
    public void test_eddsa_sign_success() {
        byte[] data = "GNUnet".getBytes();
        EddsaPrivateKey privateKey = EddsaPrivateKey.createRandom();
        EddsaPublicKey publicKey = privateKey.getPublicKey();
        EddsaSignature signature = privateKey.sign(data, 0, publicKey);
        Assert.assertTrue(signature.verify(data, 0, publicKey));
    }

    @Test
    public void test_eddsa_sign_failure() {
        byte[] data = "GNUnet".getBytes();
        EddsaPrivateKey privateKey = EddsaPrivateKey.createRandom();
        EddsaPublicKey publicKey = privateKey.getPublicKey();
        EddsaSignature signature = EddsaSignature.randomGarbage();
        Assert.assertFalse(signature.verify(data, 0, publicKey));
    }
}
