/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import org.gnunet.testing.TestingSubsystem;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author Florian Dold
 */
public class ResolverTest {
    private static final Logger logger = LoggerFactory
            .getLogger(ResolverTest.class);
    @Test
    public void test_resolver() {
        Program.configureLogging("DEBUG");
        final Wrapper<Boolean> finished1 = new Wrapper<Boolean>(true);
        final Wrapper<Boolean> finished2 = new Wrapper<Boolean>(true);

        TestingSubsystem ts = new TestingSubsystem("resolver");


        Resolver r = Resolver.getInstance();
        r.setConfiguration(ts.getConfiguration());

        r.resolveHostname("gnunet.org", RelativeTime.FOREVER, new Resolver.AddressCallback() {
            @Override
            public void onAddress(InetAddress addr) {
                logger.info("Hostname resolved to " + addr.getHostAddress());
            }

            @Override
            public void onFinished() {
                finished1.set(true);
            }

            @Override
            public void onTimeout() {
                fail();
            }
        });
        r.resolveHostname("gnu.org", RelativeTime.FOREVER, new Resolver.AddressCallback() {
            @Override
            public void onAddress(InetAddress addr) {
                logger.info("Hostname resolved to " + addr.getHostAddress());
            }

            @Override
            public void onFinished() {
                finished2.set(true);
            }

            @Override
            public void onTimeout() {
                fail();
            }
        });

        Scheduler.run();

        assertTrue(finished1.get());

        assertTrue(finished2.get());
    }
}

