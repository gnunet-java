package org.gnunet.util;


import org.gnunet.construct.MessageLoader;
import org.gnunet.construct.UInt32;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.crypto.*;
import org.junit.Assert;
import org.junit.Test;

public class HighLevelCryptoTest {

    public static class MySecretContent implements SignedContentMessage {
        @ZeroTerminatedString
        public String foo;
        @UInt32
        public int bar;
    }

    public static class MyFakeContent implements SignedContentMessage {

    }

    public void setupMessageMap() {
        MessageLoader.registerUnionCase(SignedContentMessage.class, MySecretContent.class, 1);
        MessageLoader.registerUnionCase(SignedContentMessage.class, MyFakeContent.class, 2);
    }

    @Test
    public void test_hl_ecdsa() {
        setupMessageMap();
        MySecretContent c = new MySecretContent();
        c.bar = 42;
        c.foo = "quux";
        EcdsaPrivateKey privateKey = EcdsaPrivateKey.createRandom();
        EcdsaPublicKey publicKey = privateKey.getPublicKey();
        EcdsaSignedMessage m = new EcdsaSignedMessage<MySecretContent>(c, privateKey, publicKey);

        Assert.assertTrue(m.verify(publicKey, MySecretContent.class));
    }

    @Test
    public void test_hl_ecdsa_purpose() {
        setupMessageMap();
        MySecretContent c = new MySecretContent();
        c.bar = 42;
        c.foo = "quux";
        EcdsaPrivateKey privateKey = EcdsaPrivateKey.createRandom();
        EcdsaPublicKey publicKey = privateKey.getPublicKey();
        EcdsaSignedMessage m = new EcdsaSignedMessage<MySecretContent>(c, privateKey, publicKey);


        Assert.assertFalse(m.verify(publicKey, MyFakeContent.class));
    }


    @Test
    public void test_hl_eddsa() {
        setupMessageMap();
        MySecretContent c = new MySecretContent();
        c.bar = 42;
        c.foo = "quux";
        EddsaPrivateKey privateKey = EddsaPrivateKey.createRandom();
        EddsaPublicKey publicKey = privateKey.getPublicKey();
        EddsaSignedMessage m = new EddsaSignedMessage(c, privateKey, publicKey);

        Assert.assertTrue(m.verify(publicKey, MySecretContent.class));
    }

    @Test
    public void test_hl_eddsa_purpose() {
        setupMessageMap();
        MySecretContent c = new MySecretContent();
        c.bar = 42;
        c.foo = "quux";
        EddsaPrivateKey privateKey = EddsaPrivateKey.createRandom();
        EddsaPublicKey publicKey = privateKey.getPublicKey();
        EddsaSignedMessage m = new EddsaSignedMessage(c, privateKey, publicKey);

        Assert.assertFalse(m.verify(publicKey, MyFakeContent.class));
    }

}
