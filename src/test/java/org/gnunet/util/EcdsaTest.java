/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import org.gnunet.util.crypto.EcdsaPrivateKey;
import org.gnunet.util.crypto.EcdsaPublicKey;
import org.gnunet.util.crypto.EcdsaSignature;
import org.gnunet.util.crypto.Ed25519;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

public class EcdsaTest {
    @Test
    public void test_sign_success() {
        byte[] data = "GNUnet".getBytes();
        //EcdsaPrivateKey privateKey = EcdsaPrivateKey.createRandom();
        EcdsaPrivateKey privateKey = new EcdsaPrivateKey();
        privateKey.d = Ed25519.encodeScalar(new BigInteger("9751885127070397687372377515580876465521419304255864589795046632195735740054"));
        EcdsaPublicKey publicKey = privateKey.getPublicKey();
        EcdsaSignature signature = privateKey.sign(data, 0, publicKey);
        System.out.println("private key: " + Ed25519.decodeScalar(privateKey.d));
        System.out.println("public key: " + publicKey.asPoint());
        System.out.println("signature r: " + Ed25519.decodeScalar(signature.r));
        System.out.println("signature s: " + Ed25519.decodeScalar(signature.s));
        Assert.assertTrue(signature.verify(data, 0, publicKey));
    }

    @Test
    public void test_sign_failure() {
        byte[] data = "GNUnet".getBytes();
        EcdsaPrivateKey privateKey = EcdsaPrivateKey.createRandom();
        EcdsaPublicKey publicKey = privateKey.getPublicKey();
        EcdsaSignature signature = EcdsaSignature.randomGarbage();
        Assert.assertFalse(signature.verify(data, 0, publicKey));
    }
}
