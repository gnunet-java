package org.gnunet.util;

import org.junit.Test;
import java.util.Random;
import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class PeerIdentityTest {
    @Test
    public void test_str() {
        PeerIdentity peerIdentity = new PeerIdentity();
        new Random().nextBytes(peerIdentity.data);
        PeerIdentity peerIdentity2 = PeerIdentity.fromString(peerIdentity.toString());
        Assert.assertArrayEquals(peerIdentity.data, peerIdentity2.data);
    }

    /**
     * Test that new encoding works (test string from Ilya Migal)
     */
    @Test
    public void test_ilya() {
        PeerIdentity peerIdentity = PeerIdentity.fromString("NF0QZQSD5JEAXTK701H84YP0D2K0ZRM7GYA7MN2RFBJDJPVVYM90");
        Assert.assertNotNull(peerIdentity);
    }

}

