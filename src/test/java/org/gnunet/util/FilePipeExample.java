package org.gnunet.util;

import java.io.File;
import java.io.IOError;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * ...
 *
 * @author Florian Dold
 */
public class FilePipeExample {
    public static void main(String... args) {

        Program.configureLogging("DEBUG", null);

        final Scheduler.FilePipe fp = Scheduler.openFilePipe(new File("test.pipe"));


        Scheduler.addRead(RelativeTime.FOREVER, fp.getSource(), new Scheduler.Task() {
            @Override
            public void run(Scheduler.RunContext ctx) {
                ByteBuffer b = ByteBuffer.allocate(1);
                b.clear();
                try {
                    fp.getSource().read(b);
                } catch (IOException e) {
                    throw new IOError(e);
                }
                b.flip();
                System.out.println("got: " + b.get());

                Scheduler.addRead(RelativeTime.FOREVER, fp.getSource(), this);

            }
        });

        Scheduler.run();
    }
}
