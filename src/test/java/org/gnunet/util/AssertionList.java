package org.gnunet.util;

import java.util.LinkedList;
import java.util.List;

public class AssertionList {

    List<Assertion> asyncAssertions = new LinkedList<Assertion>();


    public Assertion create(String message, Object... args) {
        Assertion assertion = new Assertion(String.format(message, args));
        asyncAssertions.add(assertion);
        return assertion;
    }

    public void assertAll() {
        for (Assertion assertion : asyncAssertions) {
            if (assertion.asserted != 1) {
                throw new AssertionError(
                        String.format("Assertion '%s' asserted %s times", assertion.message, assertion.asserted));
            }
            if (!assertion.success) {
                throw new AssertionError(
                        String.format("Assertion '%s' failed", assertion.message));
            }
        }
    }

}
