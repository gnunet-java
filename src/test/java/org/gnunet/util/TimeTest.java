package org.gnunet.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class TimeTest {

    @Test
    public void test_absolute_add_subtract() {
        AbsoluteTime t1 = AbsoluteTime.now().add(RelativeTime.FOREVER);
        Assert.assertEquals(t1, AbsoluteTime.FOREVER);
        Assert.assertTrue(t1.isForever());

        t1 = AbsoluteTime.FOREVER.add(RelativeTime.SECOND);
        Assert.assertEquals(t1, AbsoluteTime.FOREVER);
        Assert.assertTrue(t1.isForever());

        AbsoluteTime t2 = (new AbsoluteTime(123000000)).add(RelativeTime.SECOND);
        Assert.assertEquals(124000000, t2.getMicroseconds());

        AbsoluteTime t3 = (new AbsoluteTime(123000000)).subtract(RelativeTime.SECOND);
        Assert.assertEquals(122000000, t3.getMicroseconds());
    }
}
