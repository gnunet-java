/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import org.gnunet.util.crypto.EcdhePrivateKey;
import org.gnunet.util.crypto.EcdhePublicKey;
import org.gnunet.util.crypto.Ed25519;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test ECDHE.
 */
public class EcdheTest {

    /**
     * Test whether the key is correctly restored when serializing it.
     */
    @Test
    public void test_compress() {
        EcdhePrivateKey privKey1 = EcdhePrivateKey.createRandom();
        EcdhePublicKey pubKey1 = privKey1.getPublicKey();
        EcdhePublicKey pubKey2 = new EcdhePublicKey(pubKey1.asPoint());
        Assert.assertArrayEquals(pubKey1.y, pubKey2.y);
    }

    /**
     * Test a single ecdh operation.
     */
    @Test
    public void test_simple() {
        EcdhePrivateKey privKey1 = EcdhePrivateKey.createRandom();
        EcdhePrivateKey privKey2 = EcdhePrivateKey.createRandom();
        EcdhePublicKey pubKey1 = privKey1.getPublicKey();
        EcdhePublicKey pubKey2 = privKey2.getPublicKey();

        System.out.println("private key: " + Strings.dataToString(privKey1.d));
        System.out.println("private key coeff:" + Ed25519.decodeScalar(privKey1.d));
        System.out.println("public key: " + pubKey1);
        System.out.println("pubk as point: " + Ed25519.decode(pubKey1.y));


        HashCode h1 = privKey1.ecdh(pubKey2);
        HashCode h2 = privKey2.ecdh(pubKey1);

        Assert.assertArrayEquals(h1.data, h2.data);
    }

    /**
     * Test with values from the C GNUnet implementation.
     * Generated with 'gnunet-ecc -E'
     */
    @Test
    public void test_gnunet_values() {
        EcdhePrivateKey privKey1 = new EcdhePrivateKey();
        privKey1.d = new byte[32];
        EcdhePrivateKey privKey2 = new EcdhePrivateKey();
        privKey2.d = new byte[32];
        Strings.stringToData("A46TL3L8CF7R3BPPQDE8LJS999NQ58HD43DV11MPFI2B5JNJMM40", privKey1.d);
        Strings.stringToData("B60MFOBFKCVT0IVFDI800MH345FG372UJ4T7GEMNMO72O64G0D00", privKey2.d);

        EcdhePublicKey pubKey1 = privKey1.getPublicKey();
        EcdhePublicKey pubKey2 = privKey2.getPublicKey();

        Assert.assertEquals("JLU00RUGNB04APG6I9RKVT1CJLB16U8T7G130CC2IV4O3LJG5K40", pubKey1.toString());

        Assert.assertEquals("G9TDOQVP3CFA2K5Q7PSS8Q935FVEHAF3C08JPV1H3MPDK1ESLM6G", pubKey2.toString());

        HashCode h1 = privKey1.ecdh(pubKey2);
        HashCode h2 = privKey2.ecdh(pubKey1);

        Assert.assertEquals(h1, h2);

        byte[] gnunet_data = new byte[64];
        boolean success = Strings.stringToData(
                "R9A80A2VU4R0IL3NT4FBIMFRCGVP72DQODHTQQ1SGR65I4PMF1" +
                "6C3ELIF2RSB9L8H0KLOUU795IM5L0CLCISI607B5P1QE8HRAPA56O",
                gnunet_data);
        Assert.assertTrue(success);

        Assert.assertArrayEquals(gnunet_data, h1.data);

    }
}
