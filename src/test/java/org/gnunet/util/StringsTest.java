package org.gnunet.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * ...
 *
 * @author Florian Dold
 */
public class StringsTest {
    @Test
    public void test_inverse() {
        byte[] data = "GNUnet".getBytes();
        String str = Strings.dataToString(data);
        byte[] data2 = Strings.stringToData(str, data.length);
        Assert.assertArrayEquals(data, data2);
    }

    @Test
    public void test_regression() {
        String str = "00000000";
        byte[] data = new byte[5];
        Assert.assertTrue(Strings.stringToData(str, data));
    }
}

