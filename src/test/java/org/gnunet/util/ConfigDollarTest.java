/*
 This file is part of GNUnet.
  Copyright (C) 2012, 2013 Christian Grothoff (and other contributing authors)

  GNUnet is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNUnet; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;


import org.junit.Assert;
import org.junit.Test;

public class ConfigDollarTest {

    @Test
    public void test_dollar() {
        Configuration cfg = new Configuration();
        cfg.setValueString("PATHS", "FOO", "/bin/true");
        Assert.assertEquals("hello, world!", cfg.expandDollar("hello, world!"));
        Assert.assertEquals("", cfg.expandDollar("${}"));
        Assert.assertEquals("/bin/true", cfg.expandDollar("${FOO}"));
        Assert.assertEquals("/bin/true/gnu", cfg.expandDollar("${FOO}/gnu"));
        Assert.assertEquals("/bin/true/gnu", cfg.expandDollar("$FOO/gnu"));
        Assert.assertEquals("/bin/true", cfg.expandDollar("${BLUB:-/bin}/true"));
        Assert.assertEquals("/bin/true/gnu", cfg.expandDollar("${BLUB:-${FOO}}/gnu"));
        Assert.assertEquals("", cfg.expandDollar("${{{}}"));
        Assert.assertEquals("/bin/true", cfg.expandDollar("${XXX:-${XXX:-${XXX:-/bin}}}/true"));
        Assert.assertEquals("/bincd/true", cfg.expandDollar("${XXX:-${XXX:-${XXX:-/bin}c}d}/true"));
    }
}
