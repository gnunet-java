/*
 This file is part of GNUnet.
 Copyright (C) 2011, 2012 Christian Grothoff (and other contributing authors)

 GNUnet is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3, or (at your
 option) any later version.

 GNUnet is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNUnet; see the file COPYING.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 */

package org.gnunet.util;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Arrays;

import static org.gnunet.util.Server.DisconnectHandler;

/**
 * Example server implementation.
 *
 * @author Florian Dold
 */
public class ServerExample {

    public static void main(String[] args) {
        // usually servers should run inside a service, this is just for testing
        new Program() {
            @Override
            public void run() {
                Server s = new Server(Arrays.asList(new SocketAddress[]{new InetSocketAddress("127.0.0.1", 3456)}),
                        RelativeTime.MINUTE,
                        false);
                s.setHandler(new Server.MessageRunabout() {
                    public void visit(TestMessage tm) {
                        System.out.println("got TEST message");
                        final Server.ClientHandle sender = getSender();
                        sender.notifyTransmitReady(4, RelativeTime.FOREVER, new MessageTransmitter() {
                            @Override
                            public void transmit(Connection.MessageSink sink) {
                                sink.send(new TestMessage());
                                System.out.println("TEST message sent");
                                sender.receiveDone(true);
                            }

                            @Override
                            public void handleError() {
                                System.out.println("error talking to client!");
                            }
                        });
                    }

                    public void visit(UnknownMessageBody b) {
                        System.out.println("got message of unknown type " + b.id);
                    }
                });

                s.notifyDisconnect(new DisconnectHandler() {
                    @Override
                    public void onDisconnect(Server.ClientHandle clientHandle) {
                        System.out.println("client disconnected");

                    }
                });

            }
        }.start(args);
    }
}
